#!/usr/bin/env python

#####################################################
#               Initialise new System               #
#---------------------------------------------------#
# Usage: new_system.py <emailAddress> <System Type> #
#####################################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
####################################################


import time, pickle, sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from django.contrib.auth.models import User
from dash.models import System

f = open('/opt/aienv/aos/temp/user.data', 'r')
userdata = pickle.loads(f.read())
f.close()

user = User(email = userdata['email'])
print 'Email address matches database'
user.username = userdata['username']
print 'username = ' + userdata['username']
user.password = userdata['password']
print 'password = ' + userdata['password']
user.email = userdata['email']
print 'email = ' + userdata['email']
user.save()
print 'Saved User'

default = True
for s in System.objects.all():
    if s.default == true:
        default = False
system = System(sysid=userdata['sysid'])
print 'System ID = ' + userdata['sysid']
system.user = user
print 'System User = ' + user.username
system.syspass = userdata['syspass']
print 'System Password = ' + userdata['syspass']
system.systype = userdata['systype']
print 'System Type = ' + userdata['systype']
system.sysname = userdata['sysname']
print 'System Name = ' + userdata['sysname']
system.default = default
system.save()

