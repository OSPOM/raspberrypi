#!/usr/bin/env python

##############################################
#            RasPi OSPOM worker              #
##############################################
#
#  -Arduino Commands, DEALER, 127.0.0.1:3006
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['0!']              get group ID
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -receive requests over ZeroMQ
#  -process request
#  -send response back over zmq
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close, Gary Tomko
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, threading, time, os, xmlrpclib, pickle, psutil

from random import randint

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

try:
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    time.sleep(2)
except Exception, e:
    logging.info(str(e))

# set python path, import django app settings
import django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import Group, Sensor
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)

# Supervisor control
server = xmlrpclib.Server('http://localhost:9003/RPC2')

# Add raspi worker to redis
pid = os.getpid()  
logging.debug('pid ' + str(pid))
cache.set('rpi_data_pid', str(pid))
logging.debug('set pid in redis')
cache.sadd('connected_groups', 'rpi_data')

context = zmq.Context()
brokercom = context.socket(zmq.DEALER)
brokercom.setsockopt(zmq.RCVHWM, 2)
brokercom.setsockopt(zmq.SNDHWM, 2)
brokercom.setsockopt(zmq.LINGER, 100)
brokercom.setsockopt(zmq.RCVTIMEO, 5000)
brokercom.setsockopt(zmq.SNDTIMEO, 5000)
brokercom.setsockopt(zmq.IDENTITY, str(pid))   
brokercom.connect('tcp://127.0.0.1:3006')

ardusearch = context.socket(zmq.DEALER)
ardusearch.setsockopt(zmq.RCVHWM, 2)
ardusearch.setsockopt(zmq.SNDHWM, 2)
ardusearch.setsockopt(zmq.LINGER, 100)
ardusearch.setsockopt(zmq.RCVTIMEO, 3000)
ardusearch.setsockopt(zmq.SNDTIMEO, 3000)
ardusearch.setsockopt(zmq.IDENTITY, str(pid))
ardusearch.connect('tcp://127.0.0.1:3016')

logging.debug('connected to ardusearch with ID: ' + str(pid) + ' @ ' + str(time.time()))

apoller = zmq.Poller()
apoller.register(brokercom, zmq.POLLIN)

# How much to average sensor readings
sensor_arrays = {}
sensor_averages = {
    'cpu_temp':20,
    'cpu_pcnt':20,
    'mem_totl':20,
    'mem_aval':20,
    'mem_pcnt':20,
    'mem_used':20,
    'mem_free':20,
    'mem_actv':20,
    'mem_inac':20,
    'mem_buff':20,
    'mem_cach':20
}



# This is the loop that runs
cycle_count = 0  # for performace review
cycle_start = time.time()
sensor_refresh = time.time()
while 1:
    cycle_count += 1
    if cycle_count > 1000:
        cycle_duration = time.time() - cycle_start
        cycle_rate = 1000 / cycle_duration
        cycle_count = 0
        cycle_start = time.time()
        print 'cycle_rate = ' + str(cycle_rate) + '/second'
    time_now = time.time()
    if time_now > sensor_refresh:
        sensor_refresh = time_now + 5
        # Get raspi system info
        memory_info = psutil.virtual_memory()
        rpi_data = {
            'mem_totl': memory_info[0],   # Total system memory
            'mem_aval': memory_info[1],   # Available system memory
            'mem_pcnt': memory_info[2],   # Percent system memory used
            'mem_used': memory_info[3],   # Used system memory
            'mem_free': memory_info[4],   # Free system memory
            'mem_actv': memory_info[5],   # Active system memory
            'mem_inac': memory_info[6],   # Inactive system memory
            'mem_buff': memory_info[7],   # Buffered system memory
            'mem_cach': memory_info[8],   # Cached system memory
        }
        rpi_data['cpu_pcnt'] = psutil.cpu_percent()
        for i in rpi_data:
            # Add sensor value to a list in redis for averaging
            cache.rpush(i + '_array', rpi_data[i])
            if cache.llen(i + '_array') > sensor_averages[i]:
                cache.lpop(i + '_array')
            # Get the array and average all the values
            sensor_array = cache.lrange(i + '_array', 0, -1)
            sensor_value = 0
            for s in sensor_array:
                sensor_value += float(s)
            sensor_value = round(sensor_value / len(sensor_array), 2)
            cache.set(i, sensor_value, 60)
            logging.debug('set ' + i + ', ' + str(sensor_value) + ', 60 in Redis')
        # Get 1-Wire sensor data
        for d in os.listdir('/sys/bus/w1/devices/'):
            if d[0:2] != 'w1':
                logging.debug('found sensor ' + d)
                sensor_id = 's1wt' + d[-4:]
                logging.debug('sensor_id = ' + sensor_id)
                if not cache.get(sensor_id):
                    # Register new sensor
                    try:
                        Sensor.objects.get(ID=sensor_id)
                        logging.debug('found sensor ' + sensor_id)
                    except:
                        sensor = Sensor(ID=sensor_id)
                        try:
                            group = Group.objects.get(ID='RasPi000')
                        except:
                            group = Group(ID='RasPi000')
                            group.save()
                            group.active = True
                            group.name = 'RasPi'
                            group.save()
                        sensor.group = group
                        sensor.active = True
                        sensor.sensor_units = 'deg. F'
                        sensor.name = 'new 1-Wire'
                        sensor.save()
                        logging.debug('saved sensor ' + sensor_id + ' in psql')
                
                # Read data from file
                f = open('/sys/bus/w1/devices/' + d + '/w1_slave', 'r')
                lines = f.readlines()
                f.close()
                if lines[0].strip()[-3:] == 'YES':
                    temp_output = lines[1].find('t=')
                    if temp_output != -1:
                        temp_string = lines[1].strip()[temp_output+2:]
                        temp_c = float(temp_string) / 1000.0
                        temp_f = temp_c * 9.0 / 5.0 + 32.0
                        cache.rpush(sensor_id + '_array', temp_f)
                        logging.debug('raw data = ' + str(temp_f))
                        while cache.llen(sensor_id + '_array') > 20:
                            ##logging.debug('array length = ' + str(cache.llen(d + '_array')))
                            cache.lpop(sensor_id + '_array')
                        # Get the array and average all the values
                        sensor_array = cache.lrange(sensor_id + '_array', 0, -1)
                        sensor_value = 0
                        for s in sensor_array:
                            sensor_value += float(s)
                        sensor_value = round(sensor_value / len(sensor_array), 2)
                        cache.set(sensor_id, sensor_value, 60)
                        logging.debug('set ' + sensor_id + ', ' + str(sensor_value) + ', 60 in Redis')
        
        # Read float switches
        try:
            f = open('/sys/devices/virtual/gpio/gpio23/value', 'r')
            switch_state = f.read()[0]
            f.close()
            cache.rpush('srpi0000_array', int(switch_state))
            while cache.llen('srpi0000_array') > 20:
                cache.lpop('srpi0000_array')
            # Get the array and average all the values
            sensor_array = cache.lrange('srpi0000_array', 0, -1)
            sensor_value = 0
            for s in sensor_array:
                sensor_value += float(s)
            sensor_value = round(sensor_value / len(sensor_array), 2)
            cache.set('srpi0000', sensor_value, 60)
            logging.debug('set srpi0000 ' + str(sensor_value) + ', 60 in Redis')
            
        except Exception, e:
            logging.debug('float switch pin 23 not configured ' + str(e))
            
        try:
            f = open('/sys/devices/virtual/gpio/gpio24/value', 'r')
            switch_state = f.read()[0]
            f.close()
            cache.rpush('srpi0001_array', int(switch_state))
            while cache.llen('srpi0001_array') > 20:
                cache.lpop('srpi0001_array')
            # Get the array and average all the values
            sensor_array = cache.lrange('srpi0001_array', 0, -1)
            sensor_value = 0
            for s in sensor_array:
                sensor_value += float(s)
            sensor_value = round(sensor_value / len(sensor_array), 2)
            cache.set('srpi0001', sensor_value, 60)
            logging.debug('set srpi0001 ' + str(sensor_value) + ', 60 in Redis')
            
        except Exception, e:
            logging.debug('float switch pin 24 not configured ' + str(e))
        
        reset_queries()        
    
    asocks = dict(apoller.poll(100))   # checks for incoming messages for the pins
    
    if asocks.get(brokercom) == zmq.POLLIN:  # if there's a message from brokercom
        amessage = brokercom.recv_multipart()
        logging.info('brokercom: ' + str(time.time()) + '    brokercom Received: ' + str(amessage))
        
        newcmd = amessage[0]  # newcmd is 10! 11! ect
        logging.debug('brokercom: newcmd = ' + str(newcmd))
        
        clientid = amessage[1]
        logging.debug('brokercom: clientid = ' + str(clientid))
        
        web_client = amessage[2]
        logging.debug('brokercom: web_client = ' + str(web_client))
        
        if newcmd == '10!':
            rpi_response = 'rpi_data/'
            for i in rpi_data:
                rpi_response += i + ':' + str(rpi_data[i]) + ','
            rpi_response = rpi_response[:-1]
            brokercom.send_multipart(['rpi_data', rpi_response, clientid, web_client])
        else:
            brokercom.send_multipart(['rpi_data', '-1', clientid, web_client])






