#!/usr/bin/env python

#################################################################################
#          Check serial ports for changes in Arduino connection status          #
#################################################################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, redis, time, datetime, xmlrpclib, os, glob, subprocess

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

program_start_time = time.time()

import collections
# Before loading everything else, check if the Arduino port list is the same as last time
def list_arduino_ports():
    return glob.glob('/dev/ttyACM[0-9]') + glob.glob('/dev/ttyUSB*')

# Check all Arduino connections every so often
arduino_check_time = time.time()
search_interval = 60
while True:
    if arduino_check_time < time.time():
        arduino_check_time = time.time() + search_interval
        arduino_check = subprocess.check_output('/opt/ospomenv/bin/python /opt/ospomenv/ospom/arduino_search.py -vv', shell=True)
        logging.debug('arduino_check = ' + str(arduino_check))
    found_new_ports = False
    current_port_list = list_arduino_ports()
    logging.debug('current_port_list = ' + str(current_port_list))
    # get previous open port list from Redis
    cache = redis.Redis(host='localhost', port=6379, db=0)
    # Check previous ports agains current ports
    try:
        previous_port_list = cache.smembers('current_port_list')
        logging.debug('previous_port_list = ' + str(previous_port_list))
        if collections.Counter(previous_port_list) == collections.Counter(current_port_list):
            logging.debug('no change in open usb ports')
        else:
            for i in previous_port_list:
                if i not in current_port_list:
                    logging.info('usb port ' + i + ' no longer connected')
                    cache.srem('current_port_list', i)
                    logging.debug('removed ' + i + ' from current_port_list in Redis')
            for i in current_port_list:
                if i not in previous_port_list:
                    found_new_ports = True
                    logging.info('usb port ' + i + ' is now connected')
                    cache.sadd('current_port_list', i)
                    logging.debug('added ' + i + ' to current_port_list in Redis')
            if found_new_ports:
                os.system('/opt/ospomenv/ospom/arduino_search.py')
                logging.debug('running arduino_search.py')
    except Exception, e:
        logging.error('ERROR ' + str(e))
    time.sleep(1)


