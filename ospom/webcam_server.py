#!/opt/aienv/bin/env python

####################################################
#               Webcam Video Server                #
#--------------------------------------------------#
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
###################################################


import sys, os, time, logging
from cStringIO import StringIO
from SimpleCV import *

# this part sets the logging level using -v
if '-v' in sys.argv:
    level = logging.DEBUG
else:
    level = logging.INFO
logging.basicConfig(level=level, format="[%(levelname)s] %(message)s")


camx = 1280
camy = 720

logging.info('Starting getimages.py')

cam = Camera(0, {"width": camx, "height": camy})

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

myaddr = getNetworkIp()
myaddr+=':8080'
if debug: print myaddr
# start http server to stream images over local network
js = JpegStreamer(myaddr)

while True:
  # Get Image from camera
  img = cam.getImage()
  # Serve image
  img.save(js)
  logging.debug('image size = ' + str(len(img)) + ' bytes')
  # Wait before getting next image
  time.sleep(1)
  
