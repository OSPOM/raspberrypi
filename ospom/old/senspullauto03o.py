#!/usr/bin/python

#######################################
# Sensor Update Server #
#######################################
# All systems connect to the registration gateway
# at <serverip>:3001, and send up their system ID,
# system password, and local+outer IP addresses
#
# They are authorised and whitelisted to connect
# on :3002 for sensor updates, :3003 for image/video,
# and :3004 to receive commands from the server
#
##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################



# set python path, import django app settings
import sys, os
#sys.path.append('/rai/reefai/reefai')
#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#from django.conf import settings
#from system.models import System
# flush database query from memory
#from django.db import reset_queries


import zmq, time, pickle, ipgetter, socket, random

# encryption functions
from Crypto.Cipher import AES
import base64

debug = 2

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

ipi = getNetworkIp()
ipo = ipgetter.myip()

f=open('/opt/aienv/test/tempid.txt', 'r')
sysid = f.read()
f.close()
sysid = sysid.strip()
if debug > 1: print 'temp ID ' + sysid
f=open('/opt/aienv/test/temppass.txt', 'r')
syspass = f.read()
f.close()
syspass = syspass.strip()
if debug > 1: print 'temp password' + syspass

PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

context = zmq.Context()

# Registration gateway
gateway = context.socket(zmq.PUSH)
# start connections
gateway.connect("tcp://66.228.40.107:3002")

messagecount = 0
messagetime = time.time()
while 1:
  timenow = str(time.time())
  messagecount = messagecount + 1
  if messagecount > 100:
    counttime = time.time()
    messagecount = 0
    elapsedtime = counttime - messagetime
    messagetime = counttime
    if debug: '100 messages sent in ' + str(elapsedtime) + ' seconds'
  sensorinfo = {'gSkRr6Vi': ['Sundrop 1', {'etDFatWk': [str(round(random.uniform(80.4, 80.6), 2)), 'In-Tank', 'Temp', '(deg.F)', '0'],
                                           'epQfgcZW': [str(round(random.uniform(600, 620), 0)), 'In-Tank', 'PAR', '(umol m-2 s-1)', '2'],
                                           'efjik9iN': [str(round(random.uniform(0.37, 0.4), 2)), 'In-Tank', 'Flow', '(meters/s)', '1'],
                                           'elWj1rRc': ['100', 'In-Tank', 'Level', '(% Full)', 'x']}],
                'gSd9tunV': ['Level Sensor 1', {'etqAhbKz': [str(round(random.uniform(80.4, 80.6), 2)), 'Sump', 'Temp', '(deg.F)', '0'],
                                           'efUMvuC0': [str(round(random.uniform(0.35, 0.4), 2)), 'Sump', 'Flow', '(meters/s)', '2'],
                                           'elWhyFKf': [str(round(random.uniform(94, 95), 1)), 'Sump', 'Level', '(% Full)', '1']}]}


  actuatorinfo = {'gAkRr6Vi': ['4 Power Bar', {'eoR2lvJW': [str(round(random.uniform(0, 100), 0)), 'Outlet', '1', '110vac', '0'],
                                           'eob2Orc4': [str(round(random.uniform(0, 100), 0)), 'Outlet', '2', '110vac', '1'],
                                           'eo6zmDiQ': [str(round(random.uniform(0, 100), 0)), 'Outlet', '3', '110vac', '2'],
                                           'eoGrsL9b': [str(round(random.uniform(0, 100), 0)), 'Outlet', '4', '110vac', '3']}
  ]}
  
  psendS = pickle.dumps(sensorinfo)
  psendA = pickle.dumps(actuatorinfo)
  ##jsend = json.dumps(sensorinfo)
  cipher = AES.new(syspass)
  encodedS = EncodeAES(cipher, psendS)
  encodedA = EncodeAES(cipher, psendA)
  enctime = EncodeAES(cipher, timenow)
  message = [sysid, enctime, encodedS, encodedA]
  gateway.send_multipart(message)
  if debug > 1: print 'sent: ' + str(message)
  sys.stdout.flush()
  time.sleep(10)


print 'exiting and closing all connections'
gateway.close()
context.term()
print 'connections closed'
sys.exit(0)
