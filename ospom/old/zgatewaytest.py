#!/usr/bin/python

#######################################
# Encryption for registration gateway #
#######################################
# All systems connect to the registration gateway
# at <serverip>:3001, and send up their system ID,
# system password, and local+outer IP addresses
#
# They are authorised and whitelisted to connect
# on :3002 for sensor updates, :3003 for image/video,
# and :3004 to receive commands from the server
#######################################
# System info files in env/test directory:
# - sysid.txt, syspass.txt, tempid.txt, temppass.txt



# set python path, import django app settings
import sys, os
#sys.path.append('/rai/reefai/reefai')
#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#from django.conf import settings
#from system.models import System
# flush database query from memory
#from django.db import reset_queries


import zmq, redis, django, time, pickle, ipgetter, socket

# encryption functions
from Crypto.Cipher import AES
import base64

sys.path.append("/opt/aienv/aibox")
os.environ["DJANGO_SETTINGS_MODULE"] = "aibox.settings"
from django.contrib.auth.models import User
# flush database query from memory
from django.db import reset_queries
django.setup()
##from django.core.cache import caches
from dash.models import System

debug = 2

cache = redis.Redis(host='localhost', port=6379, db=0)

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

ipi = getNetworkIp()
ipo = ipgetter.myip()

s = System.objects.all()[0]
sysid = str(s.ID)
if debug: print 'sysid = ' + sysid
syspass = str(s.pswd)
if debug: print 'syspass = ' + syspass

debug = 1

PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

context = zmq.Context()
# Registration gateway
gateway = context.socket(zmq.REQ)
# start connections
gateway.connect("tcp://104.237.151.38:3001")

timenow = str(time.time())

sysinfo = {'ipi': ipi,
           'ipo': ipo,
           'timestamp': timenow}
psend = pickle.dumps(sysinfo)
##jsend = json.dumps(sysinfo)
cipher = AES.new(syspass)
encoded = EncodeAES(cipher, psend)
message = [sysid, encoded]

gateway.send_multipart(message)
print 'sent: ' + str(message)

erecvelio = gateway.recv()
##print 'received: ' + str(erecvelio)
precvelio = DecodeAES(cipher, erecvelio)
recvelio = pickle.loads(precvelio)
tempid = recvelio['tempid']
temppass = recvelio['temppass']
print 'Temporary ID: ' + tempid
print 'Temporary password: ' + temppass


cache.set(sysid + 'tempid', tempid)
cache.set(sysid + 'temppass', temppass)

print 'exiting and closing all connections'
gateway.close()
context.term()
print 'connections closed'
sys.exit(0)
