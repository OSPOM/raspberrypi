#!/opt/aienv/bin/env python 

##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


import sys, os, time, zmq, redis
from cStringIO import StringIO
from SimpleCV import *
# encryption functions
from Crypto.Cipher import AES
import base64
# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System
from django.db import reset_queries

PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 2
camx = 640
camy = 480
argnum = 0
for a in sys.argv:
    if a[0:2] == '--':
        if a[2:] == 'debug':
            debug = int(sys.argv[int(argnum)+1])
        elif a[2:] == 'size':
            camx = int(sys.argv[int(argnum)+1].split('x')[0])
            camy = int(sys.argv[int(argnum)+1].split('x')[1])
    argnum += 1

print 'Starting getimages.py @ ' + str(time.time())

if debug: print 'debug level = ' + str(debug)
if debug: print 'Camera pixels x: ' + str(camx) + ', y: ' + str(camy)
# Initialize the camera
cam = Camera(0, {"width": camx, "height": camy})

s = System.objects.all()[0]
sysid = str(s.ID)
if debug: print 'sysid = ' + sysid
syspass = str(s.pswd)
if debug: print 'syspass = ' + syspass
tempid = str(s.tempid)
if debug: print 'tempid = ' + tempid
temppass = str(s.temppass)
if debug: print 'temppass = ' + temppass
reset_queries()

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

myaddr = getNetworkIp()
myaddr+=':8080'
if debug: print myaddr
# start http server to stream images over local network
js = JpegStreamer(myaddr)

context = zmq.Context()
# Registration gateway
gateway = context.socket(zmq.PUSH)
gateway.setsockopt(zmq.HWM, 5)
# start connections
gateway.connect("tcp://66.228.40.107:3003")

lasttime = time.time() - 0.2
while True:
  timenow = time.time()
  if debug: print 'timenow: ' + str(timenow)
  tempid = str(s.tempid)
  if debug: print 'tempid = ' + tempid
  # Get Image from camera
  img = cam.getImage()
  img.save(js)
  # Upload image every 0.2 seconds
  if (time.time() - 0.2) > lasttime:
    lasttime = time.time()
    fp = StringIO()
    ##img.save('/opt/aienv/testimg.jpg')
    img.resize(400, 300).getPIL().save(fp, 'JPEG')
    data = fp.getvalue().encode("base64")
    if debug: print 'Image data: ' + str(data)
    cipher = AES.new(temppass)
    encoded = EncodeAES(cipher, data)
    enctime = EncodeAES(cipher, str(lasttime))
    message = [tempid, enctime, encoded]
    gateway.send_multipart(message)
    if debug > 1: print 'image size: ' + str(len(str(message)))
    if debug: print 'zmq image sent'
  sys.stdout.flush()
  time.sleep(0.1)
