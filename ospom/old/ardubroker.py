#!/usr/bin/env python

##############################################
#  ZeroMQ threads communicate with arduinos  #
##############################################
#
#  -Control Commands, DEALER, 127.0.0.1:3008
#      -Command Format
#          -['', <command>]
#      -Examples
#          -['', 'exit']	end program
#      -Reply Format
#          -[<reply>]
#
#  -Arduino Commands, DEALER, 127.0.0.1:3007
#      -Command Format
#          -[<group_ID>, <command>]
#      -Examples
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -send serial request to arduino, receive response
#  -send response back over zmq
#
##############################################
# License                              
#
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


debug = True
import opm

import sys, zmq, redis, time, datetime, pickle, json, opm

import sys, logging
if '-v' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(level=log_level)

cache = redis.Redis(host='localhost', port=6379, db=0)

context = zmq.Context()

arducom = context.socket(zmq.ROUTER)
arducom.setsockopt(zmq.RCVTIMEO, 10000)
arducom.setsockopt(zmq.SNDTIMEO, 10000)
arducom.setsockopt(zmq.HWM, 20)
arducom.setsockopt(zmq.LINGER, 2000)
arducom.bind('tcp://127.0.0.1:3006')

brokercom = context.socket(zmq.ROUTER)
brokercom.setsockopt(zmq.SNDTIMEO, 10000)
brokercom.setsockopt(zmq.RCVTIMEO, 10000)
brokercom.setsockopt(zmq.HWM, 20)
brokercom.setsockopt(zmq.LINGER, 2000)
brokercom.bind('tcp://127.0.0.1:3007')

mainpoller = zmq.Poller()
mainpoller.register(arducom, zmq.POLLIN)
mainpoller.register(brokercom, zmq.POLLIN)



while 1:
    socks = dict(mainpoller.poll(2000))
    
    if socks.get(brokercom) == zmq.POLLIN:
        try:
            recv_data = brokercom.recv_multipart()
            logging.debug('recv_data = ' + str(recv_data) + '  @ ' + str(time.time()))
            clientid = recv_data[0]
            logging.debug('clientid = ' + str(clientid))
            groupid = recv_data[1]
            logging.debug('groupid = ' + str(groupid))
            command = recv_data[2]
            logging.debug('command = ' + str(command))
            if groupid in cache.smembers('congroups'):
                pid = cache.get(groupid + '_pid')
                arducom.send_multipart([pid, command, clientid])
                logging.debug('sent to arducom: ' + str([pid, command, clientid]) + ' @ ' + str(time.time()))
            else:
                brokercom.send_multipart([clientid, 'ERROR, groupid: ' + groupid + ' not found!'])
                logging.debug('ERROR, groupid: ' + groupid + ' not found!')
        except Exception,e:
            logging.debug('brokercom ERROR: ' + str(e) + ' @ ' + str(time.time()))
            brokercom.send_multipart([clientid, 'ERROR: ' + str(e)])
            
    if socks.get(arducom) == zmq.POLLIN:
        try:
            recv_data = arducom.recv_multipart()
            logging.debug('recv_data = ' + str(recv_data) + '  @ ' + str(time.time()))
            groupid = recv_data[1]
            logging.debug('groupid = ' + str(groupid))
            data = recv_data[2]
            logging.debug('data = ' + str(data))
            clientid = str(recv_data[3])
            logging.debug('clientid = ' + str(clientid))
            brokercom.send_multipart([clientid, data])
            logging.debug('sent to frontend client: ' + str([clientid, data]) + ' @ ' + str(time.time()))
        except Exception,e:
            logging.debug(str(time.time()) + '    arducom ERROR: ' + str(e))
    sys.stdout.flush()

