#!/usr/bin/python

#######################################
#        Sensor Update Server         #
#######################################
# 
#
#  - Connect to default 66.228.40.107:3002 for sensor updates
#      - Message Format
#          - [<tempid>, <enctime>, <encoded>]
#      - Encoded Data Format
#          - sensor_data{ <groupname>: {<elementid>: [<value>, <name>, <type>, <units>, <order>]
#  - Connect to 127.0.0.1:3007 to receive commands
#  - Command List:
#      - ['setwait', <update_interval>]  Sets the time between sensor updates to remote server
#      - ['getwait']             Returns the sensor update interval
#
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
#######################################

import zmq, time, datetime, pickle, redis, random, sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)

# Get System info from postgres
s = System.objects.all()[0]
system_id = str(s.ID)
logging.debug('system_id = ' + system_id)
system_password = str(s.pswd)
logging.debug('system_password = ' + system_password)
reset_queries()
# Get temporary connection info from Redis
temp_id = cache.get('temp_id')
temp_pass = cache.get('temp_pass')
logging.debug('temp_id = ' + temp_id + ', temp_pass = ' + temp_pass)

context = zmq.Context()
##sensorserver = 'tcp://66.228.40.107:3002'	# Default sensor server
sensorserver = 'tcp://127.0.0.1:3061'   # Stunnel SSL
pushsocket = context.socket(zmq.PUSH)
##pushsocket.setsockopt(zmq.RCVTIMEO, 2000)
pushsocket.setsockopt(zmq.SNDTIMEO, 10000)
pushsocket.setsockopt(zmq.SNDHWM, 5)
pushsocket.connect(sensorserver)
senscon = context.socket(zmq.REP)
senscon.setsockopt(zmq.RCVTIMEO, 2000)
senscon.setsockopt(zmq.SNDTIMEO, 2000)
senscon.setsockopt(zmq.RCVHWM, 2)
senscon.bind('tcp://127.0.0.1:3031')

mainpoller = zmq.Poller()
mainpoller.register(senscon, zmq.POLLIN)

msgcount = 0
msgtime = time.time()
debugtime = time.time()
update_interval = 60	# Default to 1 message / 60 seconds
try:
    update_interval = float(cache.get('sensor_update_interval'))
except:
    cache.set('sensor_update_interval', update_interval)
    logging.debug('set sensor_update_interval to ' + str(update_interval) + ' in Redis')

while 1:
    timenow = time.time()
    if timenow > msgtime:
        update_interval = float(cache.get('sensor_update_interval'))
        msgtime = timenow + update_interval
        msgcount = msgcount + 1
        # Log sensor update statistics every 100X
        if msgcount > 100:
            counttime = time.time()
            msgcount = 0
            elapsedtime = counttime - debugtime
            debugtime = counttime
            logging.debug(datetime.datetime.now().ctime() + '   100 messages sent in ' + str(elapsedtime) + ' seconds')
        sensor_data = {}
        # Get all sensor data from active Arduino Groups
        for group in Group.objects.all().filter(active=True):
            sensor_status = {}
            for sensor in Sensor.objects.filter(display_order__gt=0).filter(group=group):
                sensor_value = cache.get(sensor.ID)
                if not sensor_value:
                    continue 
                sensor_name = sensor.name
                sensor_type = sensor.sensor_type
                sensor_units = sensor.sensor_units
                display_order = sensor.display_order
                sensor_status[sensor.ID] = {'value': sensor_value, 'name': sensor_name, 'type': sensor_type, 'units':  sensor_units, 'order': display_order]
            logging.debug('sensor_status = ' + str(sensor_status))
            sensor_data[group.ID] = {'name': group.name, 'data': sensor_status]
        pickled_msg = pickle.dumps(sensor_data)
        message = [temp_id, temp_pass, str(time.time()), pickled_msg]
        # Send data to sensor server
        pushsocket.send_multipart(message)
        logging.debug('sent: ' + str(message))
        reset_queries()

    socks = dict(mainpoller.poll(500))
    try:
        logging.debug(str(time.time()))
        if socks.get(senscon) == zmq.POLLIN:
            msg = senscon.recv_multipart()
            logging.debug(datetime.datetime.now().ctime() + '   received ' + str(msg))
            if newmsg[0] == 'setwait':
                try:
                    update_interval = float(msg[1])
                    cache.set('sensupdate_interval', update_interval)
                    usbcon.send('OK')
                    logging.debug(datetime.datetime.now().ctime() + '   set update_interval to ' + str(update_interval))
                except:
                    usbcon.send('Time ERROR, must be float ' + str(msg[1]))
                    logging.debug(datetime.datetime.now().ctime() + '   setwait ERROR, ' + str(update_interval))
            elif newmsg[0] == 'getwait':
                usbcon.send(str(update_interval))
                logging.debug(datetime.datetime.now().ctime() + '   returned update_interval: ' + str(update_interval))
            else:
                usbcon.send('ERROR: ' + str(msg))
                logging.error('Message ERROR: ' + str(msg))
    except Exception,e:
        logging.error(datetime.datetime.now().ctime() + 'MAJOR ERROR!!!' + str(e))


gateway.close()
context.term()
logging.info('exiting and closing all connections @ ' + str(time.time()))
sys.exit(0)
