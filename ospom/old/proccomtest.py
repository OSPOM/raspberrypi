#!/usr/bin/python

##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


import sys, time



def write_stdout(s):
    sys.stdout.write(s)
    sys.stdout.flush()

def write_stderr(s):
    sys.stderr.write(s)
    sys.stderr.flush()

while 1:
    write_stdout('READY\n') # transition from ACKNOWLEDGED to READY
    line = sys.stdin.readline()  # read header line from stdin
    write_stderr(line) # print it out to stderr
    headers = dict([ x.split(':') for x in line.split() ])
    data = sys.stdin.read(int(headers['len'])) # read the event payload
    write_stderr(data) # print the event payload to stderr
    write_stdout('RESULT 2\nOK') # transition from READY to ACKNOWLEDGED


