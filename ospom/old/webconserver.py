#!/usr/bin/python

#######################################
#        Web to Box connection        #
#######################################
# Persistent Zeromq REQ REP socket
# Web Server binds to 104.237.151.38:3010 with ROUTER socket
# Box connects to web server with DEALER socket
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, redis, django, time, datetime, pickle, ipgetter, socket
from Crypto.Cipher import AES
import base64

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 2

# Encryption Functions
PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

# Get System info from postgres
s = System.objects.all()[0]
sysid = str(s.ID)
if debug: print 'sysid = ' + sysid
syspass = str(s.pswd)
if debug: print 'syspass = ' + syspass
tempid = str(s.tempid)
temppass = str(s.temppass)
reset_queries()

context = zmq.Context()
webcon = context.socket(zmq.ROUTER)
##webcon.setsockopt(zmq.RCVTIMEO, 5000)
##webcon.setsockopt(zmq.SNDTIMEO, 5000)
webcon.setsockopt(zmq.HWM, 100)
##webcon.bind("tcp://104.237.151.38:3010")
webcon.bind("tcp://127.0.0.1:3010")

brokercon = context.socket(zmq.ROUTER)
##brokercon.setsockopt(zmq.SNDTIMEO, 5000)
brokercon.setsockopt(zmq.RCVTIMEO, 20000)
brokercon.setsockopt(zmq.HWM, 100)
##brokercon.bind('tcp://127.0.0.1:3007')
brokercon.bind("tcp://127.0.0.1:3011")

mainpoller = zmq.Poller()
mainpoller.register(webcon, zmq.POLLIN)
mainpoller.register(brokercon, zmq.POLLIN)
if debug: print datetime.datetime.now().ctime() + '    registered sockets with mainpoller'

exitthread = 0
while 1:
    socks = dict(mainpoller.poll(5000))
    print 'looping'
    if socks.get(brokercon) == zmq.POLLIN:
        try:
            message = brokercon.recv_multipart()
            print message

            ##brokercon.send_multipart([])
        except:
            print 'No Message'

'''
            testmsg = 'testing, testing, 123'
            cipher = AES.new(temppass)
            encodedmsg = EncodeAES(cipher, testmsg)
            time.sleep(5)
            webcon.send_multipart([tempid, encodedmsg])
            print 'sent: ' + str(encodedmsg) + ' to: ' + tempid

            clientid, encoderecv = webcon.recv_multipart()
            print 'received: ' + str(encoderecv) + '    from: ' + str(clientid)
            decoderecv = DecodeAES(cipher, encoderecv)
            print 'decodes to: ' + str(decoderecv)
'''

print 'exiting and closing all connections'
webcon.close()
context.term()
print 'connections closed'
sys.exit(0)
