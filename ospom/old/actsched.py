#!/usr/bin/env python

##############################################
#         Actuator Control Schedule          #
#--------------------------------------------#
#
#  - Access Redis for element settings
#  - Settings are specific to each element, and stored in Redis
#      Redis Keys:
#      - <elementid>nextset	next setting time as epoch
#      - <elementid>setval	current value as float
#  - Settings Schedule is stored in psql in the 'Settings' model
#  - Entries are accessed through thier 'Element' model foriegn key
#      psql Models:
#      - Settings:
#        - element, timestamp, value
#      - Element:
#        - elementid
#
#  - ZeroMQ DEALER binds to tcp127.0.0.1:3033, accepts requests for data / commands
#      Data Request Format:
#      - [<get>, <elementID>]
#      Interval Update Format:
#      - [<set>, <elementID>, <value>, <timestamp>]
#
#
##############################################
# License                              
#
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle
# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, Element, Settings
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 2
argnum = 0
for a in sys.argv:
    if a[0:2] == '--':
        if a[2:] == 'debug':
            debug = int(sys.argv[int(argnum)+1])
    argnum += 1

refreshtime = 1000 # Defaults to 1000ms (1 second) between checks for data storage
watchdogtime = time.time()

context = zmq.Context()
webcon = context.socket(zmq.REP)
webcon.setsockopt(zmq.RCVTIMEO, 20000)
webcon.setsockopt(zmq.SNDTIMEO, 20000)
webcon.setsockopt(zmq.HWM, 50)
webcon.bind("tcp://127.0.0.1:3033")

usbcon = context.socket(zmq.DEALER)
# Keep timeouts low to not interfere with sensor data storage
usbcon.setsockopt(zmq.RCVTIMEO, 10000)
usbcon.setsockopt(zmq.SNDTIMEO, 10000)
usbcon.setsockopt(zmq.HWM, 1)
usbcon.connect('tcp://127.0.0.1:3007')

zpoller = zmq.Poller()
zpoller.register(webcon, zmq.POLLIN)

# Find Active elements
elementids = {}
for group in Group.objects.filter(active=True):
    if debug > 1: print 'group: ' + group.name
    for element in Element.objects.filter(group=group):
        eid = element.elementid
        if debug: print 'Looking for Element: ' + str(eid)
        eint = cache.get(eid + 'interval')
        enext = time.time()
        if not eint:
            if debug: print 'Element: "' + eid + 'interval" not found in Redis'
            eint = 300 #defaults to 5min update interval
            cache.set(eid + 'interval', eint)
            if debug: print 'Set interval to ' + str(eint) + ' seconds'
            if debug: print 'Set next update to ' + str(enext)
        elementids[element.elementid] = [enext, eint]
        if debug: print 'Added ' + str([enext, eint]) + ' to elementids{}'
    if debug: print 'elementids = ' + str(elementids)


while 1:
    timenow = time.time()
    for e in elementids:
        nexttime = elementids[e][0]
        if debug > 2: print 'checking element ' + e + ', for next setting time: ' + str(nexttime)
        if timenow > nexttime:
            if debug: print 'getting next setting & timestamp from postgres'
            element = Element.objects.get(elementid=e)
            settingdata = Settings.objects.filter(element=element)
            setdata = []
            sorteddata = []
            for s in settingdata:
                setdata.append([s.timestamp, s.value])
                if debug: print 'setdata = ' + str(setdata)
                sorteddata = sorted(setdata, key=lambda tstamp: tstamp[0])
            if debug: print 'sorteddata = ' str(sorteddata)
            newtime = 0
            newval = 0
            for s in sorteddata:
                if timenow < s[0]:
                    newtime = s[0]
                    newval = s[1]
            if debug: print 'new setting time = ' + str(newtime)
            if debug: print 'new setting value = ' + str(newval)
            elementids[e] = [newtime, newval]
            if debug: 'elementids[' + e + '] = ' + str(elementids[e])
            try:
                nextval = float(cache.get(e))
                if debug: print 'got elementval from redis: ' + str(elementval)
                if isinstance(elementval, (int, float)):
                    if debug: print datetime.datetime.now().ctime()
                    if debug: print 'element ' + str(e) + ', value = ' + str(elementval)
                try:
                    element = Element.objects.get(elementid = str(e))
                    h = Historic(element=element)
                    h.timestamp = timenow
                    h.value = elementval
                    h.save()
                    if debug: print 'wrote data to postgres'
                except:
                    print 'ERROR!, element ID: ' + str(e)
            except:
                print datetime.datetime.now().ctime() + ' Cache ERROR!, element ID: ' + str(e)
            ##print ''
        reset_queries()

    if timenow > watchdogtime:
        watchdogtime += 60
        active = Group.objects.filter(active=True)
        if debug: print 'Active Groups:  ' + str(active)
        for group in active:
            try:
                if debug: print datetime.datetime.now().ctime() + '  checking ' + str(group.groupid)
                output = ['']
                usbcon.send_multipart([str(group.groupid), '11:1000!'])
                if debug: print 'sent ' + str([str(group.groupid), '11:1000!']) + ' to usbcon'
                output = usbcon.recv()
                if debug: print output
                if output[0]:
                    if debug: print 'received valid response' + str(output)
                else:
                    print datetime.datetime.now().ctime() + 'connection ERROR 1! group ' + str(group.groupid)
            except:
                print datetime.datetime.now().ctime() + 'ERROR! group ' + str(group.groupid)

    socks = dict(zpoller.poll(refreshtime))
    if debug > 2: print time.time()
    if socks.get(webcon) == zmq.POLLIN:
        newmsg = webcon.recv_multipart()
        if debug: print 'zpoller received ' + str(newmsg)
        if newmsg[0] == 'get':
            getids = pickle.loads(newmsg[1])
            firsttime = float(newmsg[2])
            lasttime = float(newmsg[3])
            datapoints = int(newmsg[4])
            if debug: print 'Request for elements ' + str(getids) + ' data from ' + str(firsttime) + ' to ' + str(lasttime) + ' with ' + str(datapoints) + ' datapoints'
            reqdata = []
            for e in getids:
                if debug: print 'Request for element ' + str(e) + ' data from ' + str(firsttime) + ' to ' + str(lasttime) + ' with ' + str(datapoints) + ' datapoints'
                try:
                    element = Element.objects.get(elementid = e)
                    print 'element = ' + str(element)
                    hdata = Historic.objects.filter(element=element).filter(timestamp__gt=firsttime, timestamp__lt=lasttime)
                    if debug: print 'hdata datapoints = ' + str(len(hdata))
                    sdata = []
                    for h in hdata:
                        sdata.append([h.timestamp, h.value])
                    if debug: print 'sdata = ' + str(sdata)
                    indata = sorted(sdata, key=lambda tstamp: tstamp[0])
                    timestep = (lasttime - firsttime) / datapoints
                    msg = [hdata[0].value]
                    slicetime = [firsttime, firsttime + timestep]
                    for p in range(1, datapoints):
                        slicedata = []
                        for i in indata:
                            if i[0] > float(slicetime[1]):
                                if debug: print 'found end of first slicetime @' + str(slicetime)
                                break
                            slicedata.append(i[1])
                            indata.remove(i)
                        if slicedata:
                            if debug: print 'slicedata = ' + str(slicedata)
                            hv = 0
                            for d in slicedata:
                                hv += float(d)
                            avgval = hv / len(slicedata)  
                            if debug > 1: print 'Average value for slicetime ' + str(p) + ' = ' + str(avgval) + ' out of ' + str(len(slicedata)) + ' datapoints'
                            msg += [avgval]
                        else:
                            msg += [msg[-1]]
                            if debug > 1: print 'No new data, using previos value: ' + str(msg[-1])
                        slicetime[0] += timestep
                        slicetime[1] += timestep
                    if debug: print 'msg = ' + str(msg)
                    reqdata.append(msg)
                except:
                    reqdata = ['ERROR = Group']
            pdata = pickle.dumps(reqdata)
            webcon.send(pdata)
            if debug: print 'sent  ' + str(reqdata)
                
        elif newmsg[0] == 'set':
            elementid = pickle.loads(newmsg[1])[0]
            if debug: print 'elementid = ' + str(elementid)
            try:
                element = Element.objects.get(elementid = elementid)
                updateint = float(newmsg[2])
                if debug: print 'received request to change update interval on element ' + elementid + ' to ' + str(updateint)
                cache.set(elementid + 'interval', updateint)
                elementids[elementid] = [time.time(), updateint]
                if debug: print 'New elementids{} = ' + str(elementids)
                msg = pickle.dumps(['set ' + elementid + ' update interval to ' + str(updateint)])
                webcon.send(msg)
            except Exception, e:
                msg = pickle.dumps(['ERROR = ' + str(e)])
                webcon.send(msg)
                print str(e)
        else:
            webcon.send_multipart(['ERROR = Message', newmsg])
            if debug: print 'Message Error'
    
    sys.stdout.flush()


    ##time.sleep(1)




