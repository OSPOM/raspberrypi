#!/usr/bin/python

########################################
#         Test webconserver.py         #
########################################
# send message to ZeroMQ web broker
# takes arguments (<system_id>, <message>)
# receives response from system
########################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, time, sys

debug = 2

sysid = sys.argv[1]
message = sys.argv[2]

context = zmq.Context()
brokercon = context.socket(zmq.REQ)
##webcon.connect("tcp://104.237.151.38:3010")
brokercon.connect("tcp://127.0.0.1:3011")

brokercon.send_multipart([sysid, message])
print 'sent: ' + str([sysid, message])

newmsg = brokercon.recv_multipart()
print 'received: ' + str(newmsg)

print 'exiting and closing all connections'
webcon.close()
context.term()
print 'connections closed'
sys.exit(0)
