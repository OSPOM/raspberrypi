#!/usr/bin/env python

##############################################
#   USB serial communication with Ardiuno    #
##############################################
#
#  -Arduino Commands, DEALER, 127.0.0.1:3006
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['0!']              get group ID
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -receive requests over ZeroMQ
#  -send request to Arduino over serial
#  -receive response from Arduino over serial
#  -send response back over zmq
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, threading, time, datetime, os
import serial, glob, StringIO, string, pickle, csv, json

from random import randint

import sys, logging
if '-v' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(level=log_level)

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries


def send_arduino(serial, data):
    logging.debug('serial = ' + str(serial) + '\n' + 'data = ' + str(data))
    if len(data) > 1:
        groupid = data[0]
        logging.debug('groupid = ' + groupid)
        message = data[1]
        logging.debug('message = ' + message)
    else:
        message = data[0]
        logging.debug('message = ' + message)
    resendtime = time.time() + 2
    logging.debug('resendtime = ' + str(resendtime))
    timeout = time.time() + 12
    logging.debug('timeout = ' + str(timeout))
    aresp = -1
    logging.debug('aresp = ' + str(aresp))
    messageOK = False
    logging.debug('messageOK = ' + str(messageOK))
    
    while serial.inWaiting():
        buffertrash = serial.readline()
        logging.debug('buffertrash = ' + str(buffertrash))

    while time.time() < timeout and messageOK != True:
        if message == '0!':
            serial.write(message)
            aresp = serial.readlines()[0].strip()
            logging.debug('aresp = ' + str(aresp))
            if aresp[:8] == aresp[9:]:
                logging.debug('Arduino ID: ' + aresp[9:])
                messageOK = True
                aresp = aresp[9:]
            else:
                logging.debug('ERROR1:' + aresp)
                serial.readline()
                
        else:
            logging.debug('sending ' + str(groupid) + str(message))
            serial.write(str(groupid) + str(message))
            logging.debug('wrote ' + str(groupid) + str(message))
            aresp = serial.readline().strip()
            logging.debug('Arduino message: ' + aresp)
            arespcheck = False
            arespstart = time.time()
            while time.time() < arespstart + 2 and arespcheck == False:
                if aresp[:8] == groupid:
                    messageOK = True
                    arespcheck = True
                else:
                    logging.debug('ERROR2:   ' + aresp)
                    aresp = serial.readline().strip()
    sys.stdout.flush()
    return aresp
cache = redis.Redis(host='localhost', port=6379, db=0)

##serport = ''
##serialOK = False
def open_serial():
    global serport
    serport = ''
    global serialOK
    serialOK = False
    timeout = time.time() + 60
    while serialOK != True:
        try:
            # try to get serial port info from Redis key <groupid>_port
            serport = cache.get(groupid + '_port')
            logging.debug('got serport: ' + serport + ' from redis')
            global ser
            ser = serial.Serial(port=serport, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=0.1)
            time.sleep(3)
            logging.debug('started serial com with group: ' + groupid + ', on serial port: ' + serport + ' @ ' + str(time.time()))
                sys.stdout.flush()
            recvid = send_arduino(ser, ['0!']).split('/')[0]
            logging.debug('recvid = ' + recvid)
            sys.stdout.flush()
            if recvid == groupid:
                serialOK = True
            else:
                logging.debug('ERROR: Arduino groupid = "' + groupid + '"')
                if time.time() > timeout:
                    cache.delete(groupid)
                    logging.debug('deleted ' + groupid + ' from redis')
                    cache.delete(groupid + '_pid')
                    logging.debug('deleted ' + groupid + '_pid from redis')
                    cache.srem('congroups', groupid)
                    logging.debug('removed ' + groupid + ' from congroups in redis')
                    g = Group.objects.get(ID=thisgroup)
                    g.active = False
                    g.save()
                    logging.debug('set active = False in psql Group ' + thisgroup)
                    sys.exit()
        except Exception,e:
            ser.close()
            logging.debug('Arduino Serial Exception ' + str(e))
            sys.stdout.flush()
            if time.time() > timeout:
                cache.delete(groupid)
                logging.debug('deleted ' + groupid + ' from redis')
                cache.delete(groupid + '_pid')
                logging.debug('deleted ' + groupid + '_pid from redis')
                cache.srem('congroups', groupid)
                logging.debug('removed ' + groupid + ' from congroups in redis')
                g = Group.objects.get(ID=thisgroup)
                g.active = False
                g.save()
                logging.debug('set active = False in psql Group ' + thisgroup)
                sys.exit()
            time.sleep(1)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False




pid = os.getpid()
logging.debug('pid ' + str(pid))
sys.stdout.flush()


context = zmq.Context()
brokercom = context.socket(zmq.DEALER)
brokercom.setsockopt(zmq.HWM, 2)
brokercom.setsockopt(zmq.LINGER, 100)
brokercom.setsockopt(zmq.RCVTIMEO, 5000)
brokercom.setsockopt(zmq.SNDTIMEO, 5000)
brokercom.setsockopt(zmq.IDENTITY, str(pid))
brokercom.connect('tcp://127.0.0.1:3006')

ardusearch = context.socket(zmq.DEALER)
ardusearch.setsockopt(zmq.HWM, 2)
ardusearch.setsockopt(zmq.LINGER, 100)
ardusearch.setsockopt(zmq.RCVTIMEO, 500)
ardusearch.setsockopt(zmq.SNDTIMEO, 500)
ardusearch.setsockopt(zmq.IDENTITY, str(pid))
ardusearch.connect('tcp://127.0.0.1:3016')

logging.debug('connected to ardusearch with ID: ' + str(pid) + ' @ ' + str(time.time()))

apoller = zmq.Poller()
apoller.register(brokercom, zmq.POLLIN)

# Get group ID on ZMQ from ardusearch
timenow = time.time()
timeout = timenow + 10
msgOK = False
groupid = ''
while timenow < timeout and msgOK == False:
    try:
        logging.debug('getting group ID from ardusearch' + ' @ ' + str(time.time()))
        sys.stdout.flush()
        groupid = ardusearch.recv_multipart()[1]
        logging.debug('Received groupid ' + groupid + ' over Zmq @ ' + str(time.time()))
        sys.stdout.flush()
        ardusearch.send(groupid)
        msgOK = True
    except Exception, e:
        logging.debug('groupid Error: ' + str(e) + ' @ ' + str(time.time()))
        ardusearch.send('0')
    timenow = time.time()
sys.stdout.flush()

open_serial()

while 1:
    print 'loop ' + str(time.time())
    asocks = dict(apoller.poll(2000))
    
    if asocks.get(brokercom) == zmq.POLLIN:
        amessage = brokercom.recv_multipart()
        logging.debug(datetime.datetime.now().ctime() + '    brokercom Received: ' + str(amessage))
        sys.stdout.flush()
        newcmd = amessage[0]
        logging.debug('newcmd = ' + str(newcmd))
        sys.stdout.flush()
        clientid = amessage[1]
        logging.debug('clientid = ' + str(clientid))
        sys.stdout.flush()
        aresp = ''
        try:
            aresp = send_arduino(ser, [groupid, newcmd])
            logging.debug('sending ' + str([aresp, clientid]) + ' back to ardubroker')
            sys.stdout.flush()
            brokercom.send_multipart([groupid, aresp, clientid])
        except Exception,e:
            print 'brokercom ERROR: ' + str(e)
            sys.stdout.flush()
            logging.debug('sending ' + str([str(e), clientid]) + ' back to ardubroker')
            brokercom.send_multipart([str(e), clientid])
            logging.debug('closing serial connection')
            ser.close()
            time.sleep(2)
            serport = cache.get(groupid + '_port')
            open_serial()
            logging.debug('opened new serial connection')
            sys.stdout.flush()
    
    datastream = ser.readline().strip().split('/')
    logging.debbug(datetime.datetime.now().ctime() + '    datastream = ' + str(datastream) + '\n')
    try:
        if datastream != ['']:
            if datastream[0] == groupid:
                sdatalist = datastream[1].split(',')
                logging.debug('sdatalist = ' + str(sdatalist))
                for sdata in sdatalist:
                    sensordata = sdata.split(':')
                    logging.debug('sensordata = ' + str(sensordata))
                    if len(sensordata[0]) == 8 and sensordata[0][0] == 's' and is_number(sensordata[1]):
                        cache.set(sensordata[0], sensordata[1], 60)
                        logging.debug('set ' + str(sensordata[0]) + ', ' + str(sensordata[1]) + ', 60 in Redis')
                    else:
                        print 'datastream sdata ERROR: ' + str(sdata)
            else:
                print 'datastream groupid Error: ' + str(datastream)
        sys.stdout.flush()
    except Exception,e:
        print 'datastream Error: ' + str(e) + ' @ ' + str(time.time())
        sys.stdout.flush()




