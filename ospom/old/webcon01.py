#!/usr/bin/python

#######################################
#        Web to Box connection        #
#######################################
# Persistent Zeromq REQ REP socket
# Web Server binds to 104.237.151.38:3010 with ROUTER socket
# Box connects to web server with DEALER socket
#
# Command List:
#  - ['historic', 'set', <elementID>, <updateInterval>]		set historic data update interval for specified element
#  - ['historic', 'get', <elementID>, <firstTime>, <lastTime>, <datapoints>]	get historic data for specified element
#  - ['group', 'getall', <groupID>]	get sensor data from specified arduino group
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
#######################################



import zmq, redis, django, time, datetime, pickle, subprocess
from Crypto.Cipher import AES
import base64

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 0

# Encryption Functions
PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

# Get System info from postgres
s = System.objects.all()[0]
sysid = str(s.ID)
if debug: print 'sysid = ' + sysid
syspass = str(s.pswd)
if debug: print 'syspass = ' + syspass
tempid = str(s.tempid)
if debug: print 'tempid = ' + tempid
temppass = str(s.temppass)
if debug: print 'temppass = ' + temppass
reset_queries()

lasttime = time.time()
try:
    lasttime = cache.get('webcontime')
except:
    cache.set('webcontime', lasttime)

context = zmq.Context()
webcon = context.socket(zmq.DEALER)
webcon.setsockopt(zmq.RCVTIMEO, 2000)
webcon.setsockopt(zmq.SNDTIMEO, 20000)
webcon.setsockopt(zmq.HWM, 10)
webcon.setsockopt(zmq.IDENTITY, tempid)
webcon.connect('tcp://104.237.151.38:3010')
##webcon.connect("tcp://127.0.0.1:3010")

usbcon = context.socket(zmq.DEALER)
usbcon.connect('tcp://127.0.0.1:3007')

historycon = context.socket(zmq.REQ)
historycon.setsockopt(zmq.RCVTIMEO, 20000)
historycon.setsockopt(zmq.SNDTIMEO, 2000)
historycon.connect('tcp://127.0.0.1:3030')

senscon = context.socket(zmq.REQ)
senscon.setsockopt(zmq.RCVTIMEO, 10000)
senscon.setsockopt(zmq.SNDTIMEO, 2000)
senscon.connect('tcp://127.0.0.1:3031')

mainpoller = zmq.Poller()
mainpoller.register(webcon, zmq.POLLIN)
mainpoller.register(usbcon, zmq.POLLIN)
if debug: print datetime.datetime.now().ctime() + '    registered sockets with mainpoller'

group_commands = {}
group_commands['getall'] = '10!'
group_commands['lowcal'] = '38!'
group_commands['direct'] = 'direct'

while 1:
    socks = dict(mainpoller.poll(5000))
    if debug > 2: print time.time()
    if socks.get(webcon) == zmq.POLLIN:
        newmsg = webcon.recv_multipart()

        # Decode message
        if debug: print 'received: ' + str(newmsg)
        cipher = AES.new(temppass)
        encmsg = newmsg[0]
        try:
            decmsg = DecodeAES(cipher, encmsg)
            if debug > 1: print 'decodes to: ' + str(decmsg)
        except Exception,e:
            print e
            continue
        try:
            indata = pickle.loads(decmsg)
            if debug > 1: print 'unpickled: ' + str(indata)
        except Exception,e:
            print e
            continue

        # Check message timestamp
        if debug: print 'indata = ' + str(indata)
        command = indata[0]
        if debug: print 'command = ' + str(command)
        webclientid = indata[1]
        timestamp = indata[2]
        lasttime = float(cache.get('webcontime'))
        if (time.time() + 10) > timestamp > (time.time()-10) and lasttime != timestamp:
            if debug: print 'timestamp OK: ' + str(timestamp)
        else:
            print 'timestamp ERROR: ' + str(timestamp)
            continue
        ##print 'command = ' + str(command)
        
        codeprefix = command[0]
        # Handle commands
        if codeprefix == 'group':
            reqtype = group_commands[command[1]]
            print 'reqtype = ' + str(reqtype)
            if reqtype == 'direct':     # Sent as a direct message to arduino group
                dmsg = command[2]
                groupid = dmsg[0]
                cmd = dmsg[1]
                usbcon.send_multipart([groupid, cmd])
                print 'groupid = ' + str(groupid) + ', cmd = ' + str(cmd)
            else:
                groupid = command[2]
                print 'groupid = ' + str(groupid)
                ##########################################################################
                # Add more group command modifiers here !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ##########################################################################
                usbcon.send_multipart([groupid, reqtype])
            if debug > 1: print 'sent: ' + str([groupid, reqtype]) + '   to usbcon'
            output = pickle.dumps(usbcon.recv_multipart())
            if debug > 1: print 'received: ' + str(output) + '  from usbcon'
            timenow = str(time.time())
            sendmsg = {'resp': output,
                       'timestamp': timenow,
                       'webclientid': webclientid}
            psend = pickle.dumps(sendmsg)
            encresp = EncodeAES(cipher, psend)
            webcon.send(encresp)
            if debug: print 'sent: ' + str(encresp)


        elif codeprefix == 'historic':
            try:
                reqtype = command[1]
                if reqtype == 'get':
                    elementids = pickle.dumps(command[2])
                    firsttime = command[3]
                    lasttime = command[4]
                    datapoints = command[5]
                    historycon.send_multipart([reqtype, elementids, firsttime, lasttime, datapoints])
                    if debug > 1: print 'sent: ' + str([reqtype, elementids, firsttime, lasttime, datapoints]) + '   to historycon'
                elif reqtype == 'set':
                    ##elementid = pickle.dumps(command[2])
                    elementid = pickle.dumps(command[2])
                    updatetime = command[3]
                    historycon.send_multipart([reqtype, elementid, updatetime])
                    if debug > 1: print 'sent: ' + str([reqtype, elementid, updatetime]) + '   to historycon'
                output = historycon.recv()
                if debug > 1: print 'received: ' + str(output) + '  from historycon'
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcon.send(encresp)
                if debug: print 'sent: ' + str(encresp)
            except Exception,e:
                timenow = str(time.time())
                sendmsg = {'resp': 'webcon ERROR on historycon   ' + str(e),
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcon.send(encresp)
                if debug: print str(e)
                historycon.close()
                historycon.connect('tcp://127.0.0.1:3030')

        elif codeprefix == 'run':
            runstring = command[1]
            output = subprocess.check_output(runstring, shell=True)
            if debug > 1: print 'received: ' + str(output) + '  from historycon'
            timenow = str(time.time())
            sendmsg = {'resp': output,
                       'timestamp': timenow,
                       'webclientid': webclientid}
            psend = pickle.dumps(sendmsg)
            encresp = EncodeAES(cipher, psend)
            webcon.send(encresp)
            if debug: print 'sent: ' + str(encresp)

        else:
            output = 'ERROR: odroid received: ' + str(decmsg)
            timenow = str(time.time())
            sendmsg = {'resp': output,
                       'timestamp': timenow,
                       'webclientid': webclientid}
            psend = pickle.dumps(sendmsg)
            encresp = EncodeAES(cipher, psend)
            webcon.send(encresp)
            if debug: print 'sent: ' + str(encresp)
    sys.stdout.flush()


print 'exiting and closing all connections'
##webcon.close()
context.term()
print 'connections closed'
sys.exit(0)
