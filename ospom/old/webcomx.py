#!/usr/bin/python

# saved

#######################################
#        Web to Box connection        #
#######################################
# Persistent Zeromq REQ REP socket
# Web Server binds to 104.237.151.38:3010 with ROUTER socket
# Box connects to web server with DEALER socket
#
# Command List:
#  - ['historic', 'set', <elementID>, <updateInterval>]		set historic data update interval for specified element
#  - ['historic', 'get', [<elementIDs>], <firstTime>, <lastTime>, <datapoints>]	get historic data for specified element
#  - ['group', 'getall', <groupID>]	get sensor data from specified arduino group
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, zmq.auth, redis, django, time, pickle, subprocess
from zmq.auth.thread import ThreadAuthenticator

import sys, logging
if '-v' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(level=log_level)


import ipgetter
outter_ip = ipgetter.myip()

from Crypto.Cipher import AES
import base64

# Encryption Functions
PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

def encrypt_message(plain_msg, web_id):
    global temppass
    plain_pickle = pickle.dumps(plain_msg)
    timenow = str(time.time())
    plain_packet = {'msg': plain_pickle,
                    'webclientid': web_id}
    pickled_packet = pickle.dumps(plain_packet)
    cipher = AES.new(temppass)
    encoded_packet = EncodeAES(cipher, pickled_packet)
    return encoded_packet

def decrypt_message(encrypted_msg):
    global temppass
    plain_msg = ''
    cipher = AES.new(temppass)
    try:
        plain_msg = pickle.loads(DecodeAES(cipher, encrypted_msg))
    except Exception, e:
        plain_msg = DecodeAES(cipher, encrypted_msg)
    return plain_msg

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)
webcom_pid = str(os.getpid())
cache.set('webcom_pid', webcom_pid)
logging.debug('pid = ' + webcom_pid)

# Get System info from postgres
s = System.objects.filter(default_system=True)[0]
sysid = str(s.ID)
logging.debug('sysid = ' + sysid)
syspass = str(s.pswd)
logging.debug('syspass = ' + syspass)
reset_queries()

import re
def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))


##global tempid
##global temppass

def login():
    global tempid
    global temppass
    global server_ip
    base_dir = os.path.dirname(__file__)
    keys_dir = os.path.join(base_dir, 'certificates')
    public_keys_dir = os.path.join(base_dir, 'public_keys')
    secret_keys_dir = os.path.join(base_dir, 'private_keys')

    if not (os.path.exists(keys_dir) and
            os.path.exists(public_keys_dir) and
            os.path.exists(secret_keys_dir)):
        logging.critical("Certificates are missing - run generate_certificates.py script first")
        sys.exit(1)     
    ctx = zmq.Context.instance()
    # Start an authenticator for this context.
    auth = ThreadAuthenticator(ctx)
    auth.start()
    auth.configure_curve(domain='*', location=public_keys_dir)
    client = ctx.socket(zmq.DEALER)
    client.setsockopt(zmq.LINGER, 2000)
    client.setsockopt(zmq.RCVTIMEO, 10000)
    client.setsockopt(zmq.SNDTIMEO, 10000)
    client_secret_file = os.path.join(secret_keys_dir, "client.key_secret")
    client_public, client_secret = zmq.auth.load_certificate(client_secret_file)
    client.curve_secretkey = client_secret
    client.curve_publickey = client_public
    server_public_file = os.path.join(public_keys_dir, "server.key")
    server_public, _ = zmq.auth.load_certificate(server_public_file)
    client.curve_serverkey = server_public
    # Connect to registration gateway
    client.connect('tcp://104.237.151.38:3010')
    logging.debug('connected to registration gateway @ tcp://104.237.151.38:3010')
    # Request new temporary ID and password
    msg = ['login', sysid, syspass, outter_ip]
    client.send_multipart(msg)
    logging.debug('sent ' + str(msg))
    logging.debug('waiting for temp ID')
    # Verify ID and password are valid
    try:
        response = client.recv_multipart()
        logging.debug('response = ' + str(response))
        tempid = response[0]
        temppass = response[1]
        server_ip = response[2]
        if len(tempid) == 16 and len(temppass) == 16 and is_valid_ip(server_ip) :
            logging.info('tempid = ' + tempid + '\n' + 'temppass = ' + temppass)
        else:
            logging.critical('registration ERROR1, response = ' + str(response) + ' Exiting.')
            sys.exit(0)
    except Exception, e:
        logging.critical('registration ERROR2 ' + str(e) + ' Exiting.')
        sys.exit(0)
    auth.stop()
        
def run():
    ctx = zmq.Context.instance()

    brokercom = ctx.socket(zmq.ROUTER)
    brokercom.setsockopt(zmq.RCVTIMEO, 2000)
    brokercom.setsockopt(zmq.SNDTIMEO, 20000)
    brokercom.setsockopt(zmq.LINGER, 2000)
    brokercom.setsockopt(zmq.SNDHWM, 10)
    brokercom.setsockopt(zmq.RCVHWM, 10)
    brokercom.bind('tcp://127.0.0.1:3007')
    
    historycom = ctx.socket(zmq.REQ)
    historycom.setsockopt(zmq.RCVTIMEO, 20000)
    historycom.setsockopt(zmq.SNDTIMEO, 2000)
    historycom.setsockopt(zmq.LINGER, 2000)
    historycom.setsockopt(zmq.SNDHWM, 10)
    historycom.setsockopt(zmq.RCVHWM, 10)
    historycom.connect('tcp://127.0.0.1:3030')
    
    # Move group commands to an import file !!!!!!!!!!!!!!!!!!!!!!!!
    group_commands = {}
    group_commands['getall'] = '10!'
    group_commands['lowcal'] = '38!'
    group_commands['direct'] = 'direct'
    
    
    global tempid
    global temppass
    logging.debug('tempid = ' + tempid + ' temppass = ' + temppass)
    
    global server_ip
    ##server_ip += ':3011'
    logging.debug('server_ip = ' + server_ip)
    
    webcom = ctx.socket(zmq.DEALER)
    webcom.setsockopt(zmq.IDENTITY, tempid)
    logging.debug('set client socket identity to ' + tempid)
    webcom.setsockopt(zmq.LINGER, 2000)
    webcom.setsockopt(zmq.RCVTIMEO, 10000)
    webcom.setsockopt(zmq.SNDTIMEO, 10000)
    webcom.connect('tcp://104.237.151.38:3011')
    logging.debug('connected to tcp://104.237.151.38:3011')

    # Verify connection status
    msg = 'login'
    encmsg = encrypt_message(msg, '0')
    webcom.send_multipart([encmsg])
    logging.debug('logging in to server')
    
    enc_resp = webcom.recv_multipart()
    logging.debug('enc_resp = ' + str(enc_resp))
    
    # Decode response
    response = decrypt_message(enc_resp[0])
    logging.debug('response = ' + str(response))
    msg = response['msg']
    time_stamp = response['timestamp']
    
    if msg == 'LOGIN OK':
        logging.critical('webcom client Login ERROR!')
        sys.exit(0)
    else:
        logging.info('connected to server at ip ' + server_ip)

    poller = zmq.Poller()
    poller.register(webcom, zmq.POLLIN)
    poller.register(brokercom, zmq.POLLIN)
    poller.register(historycom, zmq.POLLIN)
    heartbeat = time.time()
    starttime = time.time()
    messages_sent = 0
    
    while True:
        socks = dict(poller.poll(100))
        ##logging.debug(str(time.time()))
        if socks.get(webcom) == zmq.POLLIN:
            try:
                # handle incoming messages from OSPOM server
                encrypted_message = webcom.recv_multipart()
                logging.debug('encrypted_message = ' + str(encrypted_message))
                message = decrypt_message(encrypted_message[0])
                logging.debug('message = ' + str(message))
                web_client = message['webclientid']
                time_stamp = message['timestamp']
                msg = pickle.loads(message['msg'])
                logging.debug('msg = ' + str(msg))
                if msg == ['PING'] or msg == ['ping']:
                    encmsg = encrypt_message('PONG', web_client)
                    webcom.send_multipart([encmsg])
                    logging.debug('PONG')
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('poller ERROR! runtime = ' + runtime + '   ' + str(e))
                sys.exit(0)
        
        if socks.get(brokercom) == zmq.POLLIN:
            try:
                message = brokercom.recv_multipart()
                logging.debug('encrypted_message = ' + str(encrypted_message))
                decrypted_message = decrypt_message(encrypted_message[0])
                logging.debug('decrypted_message = ' + decrypted_message)
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('poller ERROR! runtime = ' + runtime + '   ' + str(e))
                sys.exit(0)
         
        if time.time() > heartbeat + 20:
            heartbeat = time.time()
            enc_hb = encrypt_message(heartbeat, '1')
            webcom.send_multipart([str(enc_hb)])
            messages_sent += 1
            logging.debug('heartbeat ' + str(messages_sent) + '  ' + str(enc_hb))
            



if __name__ == '__main__':
    if zmq.zmq_version_info() < (4,0):
        raise RuntimeError("Security is not supported in libzmq version < 4.0. libzmq version {0}".format(zmq.zmq_version()))

    if '-v' in sys.argv:
        level = logging.DEBUG
    else:
        level = logging.INFO

    #logging.basicConfig(level=level, format="[%(levelname)s] %(message)s")

    login()
    
    run()






'''
        while 1:
            socks = dict(mainpoller.poll(5000))
            if debug > 2: print time.time()
            if socks.get(webcom) == zmq.POLLIN:
                newmsg = webcom.recv_multipart()

                # Decode message
                if debug: print 'received: ' + str(newmsg)
                cipher = AES.new(temppass)
                encmsg = newmsg[0]
                try:
                    decmsg = DecodeAES(cipher, encmsg)
                    if debug > 1: print 'decodes to: ' + str(decmsg)
                except Exception,e:
                    print e
                    continue
                try:
                    indata = pickle.loads(decmsg)
                    if debug > 1: print 'unpickled: ' + str(indata)
                except Exception,e:
                    print e
                    continue
                    
            # Check message timestamp
            if debug: print 'indata = ' + str(indata)
            command = indata[0]
            if debug: print 'command = ' + str(command)
            webclientid = indata[1]
            timestamp = indata[2]
            lasttime = float(cache.get('webcomtime'))
            if (time.time() + 10) > timestamp > (time.time()-10) and lasttime != timestamp:
                if debug: print 'timestamp OK: ' + str(timestamp)
            else:
                print 'timestamp ERROR: ' + str(timestamp)
                continue
            ##print 'command = ' + str(command)
        
            codeprefix = command[0]
            # Handle commands
            if codeprefix == 'actuator':
                reqtype = command[1]
                print 'reqtype = ' + str(reqtype)
                if reqtype == 'getlist':     # Get active displayed Actuator list from database
                    try:
                        groups = Group.objects.filter(active=True)
                        elements = []
                        for g in groups:
                            elements.extend(Element.objects.filter(group = g))
                        print 'elements = ' + str(elements)
                        actlist = {}
                        for e in elements:
                            if e.elementid[0] == 'a' and e.order > 0 and e.active == True:
                                actlist[e.elementid] = [e.name, e.etype, e.eunits, e.order]
                        print 'actlist = ' + str(actlist)
                        pactlist = pickle.dumps(actlist)
                        timenow = str(time.time())
                        sendmsg = {'resp': pactlist,
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'sent: ' + str(encresp)
                    except Exception,e:
                        timenow = str(time.time())
                        sendmsg = {'resp': str(e),
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'ERROR sent: ' + str(encresp)
                        
                elif reqtype == 'getdata':
                    try:
                        elementids = command[3].replace('[', '').replace(']', '').split(',')
                        print 'elementids = ' + str(elementids)
                        groupids = []
                        for elementid in elementids:
                            if debug: print 'elementid = ' + elementid
                            element = Element.objects.get(elementid=elementid)
                            if debug: print 'element = ' + str(element)
                            group = element.group
                            if debug: print 'group = ' + str(group)
                            groupid = group.groupid
                            if debug: print 'groupid = ' + groupid
                            if groupid not in groupids:
                                if debug: print 'groupid ' + groupid + ' not found in groupids'
                                groupids.append(str(groupid))
                                if debug: print 'added ' + groupid + 'to groupids'
                            else:
                                if debug: print 'groupid ' + groupid + 'found in groupids'
                        if debug: print 'groupids = ' + str(groupids)
                        adata = []
                        for groupid in groupids:
                            brokercom.send_multipart([groupid, '13!'])
                            if debug > 1: print 'sent: ' + str([groupid, '13!']) + '   to brokercom'
                            output = brokercom.recv().split('/')
                            if debug: print 'output = ' + str(output)
                            adata.extend(output[1].split(','))
                        if debug: print 'adata = ' + str(adata)
                        padata = pickle.dumps(adata)
                        sendmsg = {'resp': padata,
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'sent getdata: ' + str(encresp)
                    except Exception,e:
                        timenow = str(time.time())
                        sendmsg = {'resp': str(e),
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'ERROR sent: ' + str(encresp)
            
            elif codeprefix == 'group':
                reqtype = group_commands[command[1]]
                print 'reqtype = ' + str(reqtype)
                if reqtype == 'direct':     # Sent as a direct message to arduino group
                    dmsg = command[2]
                    groupid = dmsg[0]
                    cmd = dmsg[1]
                    brokercom.send_multipart([groupid, cmd])
                    print 'groupid = ' + str(groupid) + ', cmd = ' + str(cmd)
                else:
                    groupid = command[2]
                    print 'groupid = ' + str(groupid)
                    ##########################################################################
                    # Add more group command modifiers here !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    ##########################################################################
                    brokercom.send_multipart([groupid, reqtype])
                if debug: print 'sent: ' + str([groupid, reqtype]) + '   to brokercom'
                output = pickle.dumps(brokercom.recv_multipart())
                if debug: print 'received: ' + str(output) + '  from brokercom'
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)


            elif codeprefix == 'historic':
                try:
                    reqtype = command[1]
                    if reqtype == 'get':
                        elementids = pickle.dumps(command[2])
                        firsttime = command[3]
                        lasttime = command[4]
                        datapoints = command[5]
                        historycom.send_multipart([reqtype, elementids, firsttime, lasttime, datapoints])
                        if debug > 1: print 'sent: ' + str([reqtype, elementids, firsttime, lasttime, datapoints]) + '   to historycom'
                    elif reqtype == 'set':
                        ##elementid = pickle.dumps(command[2])
                        elementid = pickle.dumps(command[2])
                        updatetime = command[3]
                        historycom.send_multipart([reqtype, elementid, updatetime])
                        ##if debug > 1: print 'sent: ' + str([reqtype, elementid, updatetime]) + '   to historycom'
                    output = historycom.recv()
                    ##if debug > 1: print 'received: ' + str(output) + '  from historycom'
                    timenow = str(time.time())
                    sendmsg = {'resp': output,
                               'timestamp': timenow,
                               'webclientid': webclientid}
                    psend = pickle.dumps(sendmsg)
                    encresp = EncodeAES(cipher, psend)
                    webcom.send(encresp)
                    ##if debug: print 'sent: ' + str(encresp)
                except Exception,e:
                    timenow = str(time.time())
                    sendmsg = {'resp': 'webcom ERROR on historycom   ' + str(e),
                               'timestamp': timenow,
                               'webclientid': webclientid}
                    psend = pickle.dumps(sendmsg)
                    encresp = EncodeAES(cipher, psend)
                    webcom.send(encresp)
                    if debug: print str(e)
                    historycom.close()
                    historycom.connect('tcp://127.0.0.1:3030')

            elif codeprefix == 'run':
                runstring = command[1]
                output = subprocess.check_output(runstring, shell=True)
                if debug > 1: print 'received: ' + str(output) + '  from historycom'
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)

            else:
                output = 'ERROR: odroid received: ' + str(decmsg)
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)
        sys.stdout.flush()


print 'exiting and closing all connections'
##webcom.close()
context.term()
print 'connections closed'
sys.exit(0)

'''
