#!/usr/bin/env python

# A process which emits a process communications event on its stdout,
# and subsequently waits for a line to be sent back to its stdin by
# loop_listener.py.
##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################



import time, sys
from supervisor import childutils


while 1:
    childutils.pcomm.stdout('testing')
    ##print str(time.time()) + ' testing'
    sys.stdout.flush()
    time.sleep(5)
