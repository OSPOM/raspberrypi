#!/usr/bin/env python

##############################################
# zmq threads communicate with arduinos
#---------------------------------------------
#
# take requests over zmq
#  -Control Commands, DEALER, 127.0.0.1:3008
#    -['', 'exit']	end program
#  -Arduino Commands, DEALER, 127.0.0.1:3008
#    -[<groupID>, <command>]
#
#  -send serial request to arduino, receive response
#  -send response back over zmq
#
#  -read/write to django database + redis
#
#  -check bus for arduinos
#  -create new database entries if needed
#  
# 
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import aos, zmq, redis, threading, time
import serial, glob, StringIO, string, pickle, csv, json

from random import randint

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System

cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 0
argnum = 0
for a in sys.argv:
    if a[0:1] == '-':
        if a[1:] == 'v':
            debug = int(sys.argv[int(argnum)+1])
        else:
            print 'unknown command: ' + a[0:]
    argnum += 1

exitthread = 0
'''
def log2file(name, mesg):
    f = open('/var/log/aos/usbcon/' + name, 'a')
    f.write(mesg)
    f.close()

def is_number(s):
    try:
        float(s)
        return True
    except Exception,e:
    ##except ValueError:  # this didn't handle a list out of range error, and crashed arduino thread
        return False

def list_arduino_ports():
    return glob.glob('/dev/ttyACM[0-9]') + glob.glob('/dev/ttyUSB*')

def arduino_serial(serial, message):
    serial.write(message)
    resendtime = time.time() + 3
    timeout = time.time() + 9
    aresp = ''
    messageOK = False
    lastmessage = ''
    while time.time() < timeout and messageOK != True:
            aresp = serial.readline().strip()
            if aresp != '':
            ##if aresp != '-1' and len(aresp) > 7:
                if aresp == lastmessage:
                    if debug: print 'Arduino message: ' + aresp
                    messageOK = True
                else:
                    lastmessage = aresp
                    if debug: print 'received ' + aresp
                    serial.write(message)
            elif time.time() < resendtime:
                serial.write(message)
                resendtime = time.time() + 3
    return aresp

def query_arduino(portname):
    if debug:
        print 'running query_arduino() on ' + portname
    log2file('query_arduino', 'running query_arduino() on ' + portname + '\n')
    ser = serial.Serial(port=portname, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=3)
    mygroup = ''
    time.sleep(4)
    aresp = arduino_serial(ser, '0!')
    if len(aresp) == 8 and aresp[0] == 'g':
        if debug:
            print 'valid group'
        log2file('query_arduino', 'valid group' + '\n')
        mygroup = str(aresp)
        gdes = mygroup[1:3]
        grouptype = ''
        for gtype in GroupTypes.objects.all():
            if gtype.designator == gdes:
                grouptype = gtype.name
                if debug > 1:
                    print 'found group type: "' + grouptype + '" in database'
                log2file('query_arduino', 'found group type: "' + grouptype + '" in database' + '\n')
        newgroup = 0
        if mygroup in dbgroups:
            if debug > 1:
                print 'found ' + mygroup + ' in dbgroups, setting active flag'
            log2file('query_arduino', 'found ' + mygroup + ' in dbgroups, setting active flag' + '\n')
            g = Group.objects.get(groupid = mygroup)
            g.active = True
            g.save()
        else:
            if debug > 1:
                print mygroup + ' not found in dbgroups, creating new entry in psql'
                log2file('query_arduino', mygroup + ' not found in dbgroups, creating new entry in psql' + '\n')
            g = Group(groupid = mygroup)
            g.gtype = grouptype
            g.name = grouptype + ' ' + mygroup
            g.active = True
            g.system = System.objects.get(sysid=sysid)
            g.save()
            newgroup = 1
        congroups.append(mygroup)
        groupports[mygroup] = portname
        if newgroup:
            if debug:
                print 'getting element IDs'
            log2file('query_arduino', 'getting element IDs' + '\n')
            aresp = arduino_serial(ser, mygroup + '10!')
            aresp = aresp.split('/')
            grpid = aresp[0]
            for edat in aresp[1].split(','):
                eid = edat.split(':')[0]
                edes = eid[1:3]
                elval = edat.split(':')[1]
                if debug: 
                    print 'eid: ' + eid + ', val= ' + elval
                log2file('query_arduino', 'eid: ' + eid + ', val= ' + elval + '\n')
                if not Element.objects.filter(elementid=eid).exists():
                    if debug:
                        print 'creating new database entry for Element: ' + eid
                    log2file('query_arduino', 'creating new database entry for Element: ' + eid + '\n')
                    e = Element(elementid=eid)
                    for eltype in ElementTypes.objects.all():
                        if eltype.designator == edes:
                            e.name = eltype.name + ' ' + eid
                            e.etype = eltype.name
                            if debug > 1:
                                print 'accessing Group: ' + mygroup + ', with name: ' + g.name
                            log2file('query_arduino', 'accessing Group: ' + mygroup + ', with name: ' + g.name + '\n')
                            g = Group.objects.get(groupid = mygroup)
                            e.group = g
                            e.save()
        ser.close()
        thread = threading.Thread(target=arduino_thread, args=(mygroup,))
        thread.start()
        print 'dbgroups: ' + str(dbgroups)
        return True
    else:
        return False

def find_arduinos():
    print 'starting find_arduinos()'
    ##time.sleep(20)
    global exitthread
    while not exitthread:
        if debug: print 'running find_arduinos() ' + str(time.time()) 
        openports = list_arduino_ports()
        if debug > 1: print "openports = " + str(openports)
        dbgroups = []
        for i in Group.objects.all():
            gid = i.groupid.encode('ascii','ignore')
            dbgroups.append(gid)
        if debug > 1: 
            print 'dbgroups = ' + str(dbgroups)
            print 'congroups = ' + str(congroups)
            print 'elementIDs = ' + str(elementIDs)
            print 'groupports = ' + str(groupports)
        if debug > 1: print 'checking for newly connected arduinos'
        newports = list(openports)
        for i in groupports:
            thisport = groupports[i]
            if debug > 1: print 'thisport = ' + thisport
            if thisport in openports:
                if debug > 1: print thisport + ' still connected'
                newports.remove(thisport)
            else:
                if debug: print str(groupports[i]) + ' no longer connected'
        if debug and newports != []: print 'newports = ' + str(newports)
        for i in newports:
            foundgroup = query_arduino(i)
            if foundgroup:
                if debug: print 'added new group at ' + i
        deactivatedports = []
        for i in groupports:
            if groupports[i] not in openports:
                deactivatedports.append(i)
        if debug and deactivatedports != []: print 'deactivatedports ' + str(deactivatedports)
        for i in deactivatedports:
            del groupports[i]
            congroups.remove(i)
            g = Group.objects.get(groupid = i)
            g.active = False
            g.save()
        global arduinosready
        arduinosready = True
        time.sleep(30)


def arduino_thread(groupid, context=None):
    global exitthread
    arduport = groupports[groupid]
    if debug:
        print 'starting group ' + groupid + ' thread  @ ' + arduport
        log2file('thread.' + groupid, 'starting group ' + groupid + ' thread  @ ' + arduport + '\n')
    threadID = groupid + ' ' + str(randint(0,999))
    ser = serial.Serial(port=arduport, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=3)
    context = context or zmq.Context.instance()
    arducon = context.socket(zmq.DEALER)
    arducon.setsockopt(zmq.IDENTITY, groupid)
    arducon.setsockopt(zmq.HWM, 10)
    arducon.connect('tcp://127.0.0.1:3006')
    threadpoller = zmq.Poller()
    threadpoller.register(arducon, zmq.POLLIN)
    while True:
        if debug: print 'thread ' + threadID + ' running...'
        if exitthread:
            return
        try:
            asocks = dict(threadpoller.poll(100))
        except KeyboardInterrupt:
            return
        if asocks.get(arducon) == zmq.POLLIN:
            amessage = arducon.recv_multipart()
            if debug:
                print 'Thread Received: ' + str(amessage)
                log2file('thread.' + groupid, 'Thread Received: ' + str(amessage) + '\n')
            aresp = ''
            if amessage[0] == 'exit':
                 return
            try:
                aresp = arduino_serial(ser, amessage[0])
            except Exception,e:
                print 'arduino_thread() inner ERROR: ' + groupid + ' at ' + arduport + '\n' + str(e)
                log2file('thread.' + groupid, 'arduino_thread() inner ERROR: ' + groupid + ' at ' + arduport + '\n' + str(e) + '\n')
            arducon.send_multipart([aresp, amessage[1]])
            if debug > 1:
                print 'thread sent back' + aresp + ', ' + amessage[1]
                log2file('thread.' + groupid, 'thread sent back: ' + aresp + ', ' + amessage[1] + '\n')
        try:
            datastream = ser.readline().strip().split('/')
            ##if datastream != [''] and len(datastream) != 8:
            ##if datastream != [''] and len(datastream) > 8:
            if datastream != [''] and len(datastream[1]) > 9:
                if debug > 1:
                    print 'datastream: ' + str(datastream)
                    log2file('thread.' + groupid, 'datastream: ' + str(datastream) + '\n')
                if datastream[0] == groupid:
                    edatalist = datastream[1].split(',')
                    for edata in edatalist:
                        elementdata = edata.split(':')
                        if len(elementdata[0]) == 8 and elementdata[0][0] == 'e' and is_number(elementdata[1]):
                            if debug:
                                print 'writing element data ' + str(elementdata) + ' to cache'
                                log2file('thread.' + groupid, 'writing element data ' + str(elementdata) + ' to cache' + '\n')
                            cache.set(elementdata[0], elementdata[1], 60)
                        else:
                            print 'edata ERROR: ' + str(edata)
                            log2file('thread.' + groupid, 'edata ERROR: ' + str(edata) + '\n')
                else:
                    print 'datastream Error: ' + str(datastream)
                    log2file('thread.' + groupid, 'datastream Error: ' + str(datastream) + '\n')
        except Exception,e:
            print 'datastream ERROR: ' + str(e)
            log2file('thread.' + groupid, 'datastream ERROR: ' + str(e) + '\n')
'''
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
print 'Starting usbcon'

sysid = ''
for s in System.objects.all():
    if s.default:
        sysid = s.ID
        break

context = zmq.Context()

threadcon = context.socket(zmq.ROUTER)
threadcon.setsockopt(zmq.RCVTIMEO, 5000)
threadcon.setsockopt(zmq.SNDTIMEO, 5000)
threadcon.setsockopt(zmq.HWM, 100)
threadcon.bind('tcp://127.0.0.1:3006')

brokercon = context.socket(zmq.ROUTER)
brokercon.setsockopt(zmq.SNDTIMEO, 5000)
brokercon.setsockopt(zmq.RCVTIMEO, 5000)
brokercon.setsockopt(zmq.HWM, 100)
brokercon.bind('tcp://127.0.0.1:3007')

controlcon = context.socket(zmq.REP)
controlcon.setsockopt(zmq.SNDTIMEO, 5000)
controlcon.setsockopt(zmq.RCVTIMEO, 5000)
controlcon.setsockopt(zmq.HWM, 10)
controlcon.bind('tcp://127.0.0.1:3008')

mainpoller = zmq.Poller()
mainpoller.register(threadcon, zmq.POLLIN)
mainpoller.register(brokercon, zmq.POLLIN)
mainpoller.register(controlcon, zmq.POLLIN)
if debug: print "registered sockets with mainpoller"

dbgroups = []
for i in Group.objects.all():
    dbgroups.append(i.groupid.encode('ascii','ignore'))
if debug: print 'dbgroups: ' + str(dbgroups)
congroups = []
elementIDs = []
groupports = {}
ports = aos.list_arduino_ports()
if debug: print 'Found active ports: ' + str(ports)

arduinosready = False
thread = threading.Thread(target=aos.find_arduinos, args=())
thread.start()

threadstarttime = time.time() + 20

while not exitthread:
    socks = dict(mainpoller.poll(2000))
    if socks.get(controlcon) == zmq.POLLIN:
        try:
            control = controlcon.recv_multipart()
            if debug: print "received on controlcon: " + str(control)
            if control == ['exit']:
                controlcon.send('exiting')
                exitthread = 1
            else:
                controlcon.send('ERROR, command not found!')
        except Exception,e:
            print 'controlcon ERROR: ' + str(e)

    if socks.get(brokercon) == zmq.POLLIN:
        try:
            clientid, groupid, command = brokercon.recv_multipart()
            if debug:
                print 'received on brokercon: ' + str([clientid, groupid, command])
                aos.log2file('thread.' + groupid, 'received on brokercon: ' + str([clientid, groupid, command]) + '\n')
            print 'arduinosready = ' + str(arduinosready)
            if arduinosready:
                if groupid in congroups:
                    threadcon.send_multipart([groupid, groupid + command, clientid])
                    if debug:
                        print 'sent to thread: ' + str([groupid, command, clientid])
                        aos.log2file('thread.' + groupid, 'sent to thread: ' + str([groupid, command, clientid]) + '\n')
                else:
                    brokercon.send_multipart([clientid, 'ERROR, group not found!'])
                    aos.log2file('thread.' + groupid, 'ERROR, group not found!' + '\n')
            else:
                brokercon.send_multipart([clientid, 'wait'])
                aos.log2file('thread.' + groupid, 'wait' + '\n')
        except Exception,e:
            print 'brokercon ERROR: ' + str(e)

    if socks.get(threadcon) == zmq.POLLIN:
        try:
            groupid, data, clientid = threadcon.recv_multipart()
            if debug:
                print "received from thread: " + str([groupid, data, clientid])
                aos.log2file('thread.' + groupid, 'threadcon received from thread: ' + str([groupid, data, clientid]) + '\n')
            brokercon.send_multipart([clientid, data])
            if debug:
                print "sent to frontend client: " + str([clientid, data])
                aos.log2file('thread.' + groupid, 'threadcon sent to frontend client: ' + str([clientid, data]) + '\n')
        except Exception,e:
            print 'threadcon ERROR: ' + str(e)

    if debug: print 'mainpoller...'
    sys.stdout.flush()


exitthread = 1
