#!/usr/bin/env python

##############################################
#  ZeroMQ threads communicate with arduinos  #
##############################################
#
#  -Control Commands, DEALER, 127.0.0.1:3008
#      -Command Format
#          -['', <command>]
#      -Examples
#          -['', 'exit']	end program
#      -Reply Format
#          -[<reply>]
#
#  -Arduino Commands, DEALER, 127.0.0.1:3007
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -send serial request to arduino, receive response
#  -send response back over zmq
#
#  -read/write to django database + redis
#
#  -check bus for arduinos
#  -create new database entries if needed
#  
# 
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, redis, threading, time, datetime
import serial, glob, StringIO, string, pickle, csv, json

from random import randint

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System
from django.db import reset_queries
'''
import xmlrpclib
supervisorduri = 'http://localhost:9003/RPC2'
supserv = xmlrpclib.Server(supervisorduri)
supserv.supervisor.startProcess('historycon')
'''
cache = redis.Redis(host='localhost', port=6379, db=0)

##debug = ['query', 'find', 'serial']
debug = ['arduino_serial', 'query_arduino', 'find_arduinos', 'arduino_thread', 'main', 'controlcon', 'brokercon']
argnum = 0
for a in sys.argv:
    if a[0:1] == '-':
        if a[1:] == 'v':
            debug = sys.argv[int(argnum)+1].split(',')
        ##else:
            ##printdatetime.datetime.now().ctime() + '    unknown command: ' + a[0:]
    argnum += 1

exitthread = 0

def log2file(name, mesg):
    f = open('/var/log/aos/usbcon/' + name, 'a')
    f.write(mesg + '\n')
    f.close()

def is_number(s):
    try:
        float(s)
        return True
    except Exception,e:
    ##except ValueError:  # this didn't handle a list out of range error, and crashed arduino thread
        return False

def list_arduino_ports():
    return glob.glob('/dev/ttyACM[0-9]') + glob.glob('/dev/ttyUSB*')

def arduino_serial(serial, groupid, message):
    if 'arduino_serial' in debug:
        ##print'groupid = ' + str(groupid)
        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     groupid = ' + str(groupid))
    if 'arduino_serial' in debug:
        ##print'message = ' + str(message)
        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     message = ' + str(message))
    resendtime = time.time() + 2
    timeout = time.time() + 12
    aresp = -1
    messageOK = False
    while serial.inWaiting():
        buffertrash = serial.readline()
        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     buffertrash = ' + str(buffertrash))
    while time.time() < timeout and messageOK != True:
        if message == '0!':
            serial.write(message)
            aresp = serial.readlines()
            if 'arduino_serial' in debug: 
                ##print'aresp = ' + str(aresp)
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     aresp = ' + str(aresp))
            aresp = aresp[0].strip()
            if 'arduino_serial' in debug:
                ##print'aresp now = ' + aresp
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     aresp now = ' + aresp)
            if aresp[:8] == aresp[9:]:
                if 'arduino_serial' in debug:
                    ##printdatetime.datetime.now().ctime() + '    Arduino ID: ' + aresp[9:]
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '     groupid = ' + str(groupid))
                messageOK = True
                aresp = aresp[9:]
            else:
                if 'arduino_serial' in debug:
                    ##printdatetime.datetime.now().ctime() + '    ERROR1:' + aresp
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    ERROR1:' + aresp)
                serial.readline()
        else:
            serial.write(groupid + message)
            aresp = serial.readline().strip()
            arespcheck = False
            arespstart = time.time()
            while time.time() < arespstart + 2 and arespcheck == False:
                if aresp[:8] == groupid:
                    if 'arduino_serial' in debug:
                        ##printdatetime.datetime.now().ctime() + '    Arduino message: ' + aresp
                        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    Arduino response = ' + aresp)
                    messageOK = True
                    arespcheck = True
                else:
                    if 'arduino_serial' in debug:
                        ##printdatetime.datetime.now().ctime() + '    ERROR2:   ' + aresp
                        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    Serial ERROR: ' + aresp)
                    aresp = serial.readline().strip()
    return aresp


def query_arduino(portname):
    if 'query_arduino' in debug:
        ##printdatetime.datetime.now().ctime() + '    running query_arduino() on ' + portname
        log2file('query_arduino', datetime.datetime.now().ctime() + '    running query_arduino() on ' + portname + '\n')
    ser = serial.Serial(port=portname, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=2)
    mygroup = ''
    time.sleep(4)
    aresp = arduino_serial(ser, '', '0!')
    try:
        if len(aresp) == 8 and aresp[0] == 'g':
            if 'query_arduino' in debug:
                ##printdatetime.datetime.now().ctime() + '    valid group'
                log2file('query_arduino', datetime.datetime.now().ctime() + '    valid group' + '\n')
            mygroup = str(aresp)
            gdes = mygroup[1:3]
            grouptype = ''
            for gtype in GroupTypes.objects.all():
                if gtype.designator == gdes:
                    grouptype = gtype.name
                    if 'query_arduino' in debug:
                        ##printdatetime.datetime.now().ctime() + '    found group type: "' + grouptype + '" in database'
                        log2file('query_arduino', datetime.datetime.now().ctime() + '    found group type: "' + grouptype + '" in database' + '\n')
            newgroup = 0
            if mygroup in dbgroups:
                if 'query_arduino' in debug:
                    ##printdatetime.datetime.now().ctime() + '    found ' + mygroup + ' in dbgroups, setting active flag'
                    log2file('query_arduino', datetime.datetime.now().ctime() + '    found ' + mygroup + ' in dbgroups, setting active flag' + '\n')
                g = Group.objects.get(groupid = mygroup)
                g.active = True
                g.save()
            else:
                if 'query_arduino' in debug:
                    ##printdatetime.datetime.now().ctime() + '    ' + mygroup + ' not found in dbgroups, creating new entry in psql'
                    log2file('query_arduino', datetime.datetime.now().ctime() + '  ' + mygroup + ' not found in dbgroups, creating new entry in psql' + '\n')
                g = Group(groupid = mygroup)
                g.gtype = grouptype
                g.name = grouptype + ' ' + mygroup
                g.active = True
                g.system = System.objects.get(ID=sysid)
                g.save()
                newgroup = 1
            congroups.append(mygroup)
            groupports[mygroup] = portname
            if newgroup:
                if 'query_arduino' in debug:
                    ##printdatetime.datetime.now().ctime() + '    getting element IDs'
                    log2file('query_arduino', datetime.datetime.now().ctime() + '    getting element IDs' + '\n')
                aresp = arduino_serial(ser, mygroup, '10!')
                aresp = aresp.split('/')
                grpid = aresp[0]
                for edat in aresp[1].split(','):
                    eid = edat.split(':')[0]
                    edes = eid[1:3]
                    elval = edat.split(':')[1]
                    if 'query_arduino' in debug: 
                        ##printdatetime.datetime.now().ctime() + '    eid: ' + eid + ', val= ' + elval
                        log2file('query_arduino', datetime.datetime.now().ctime() + '    eid: ' + eid + ', val= ' + elval + '\n')
                    if not Element.objects.filter(elementid=eid).exists():
                        if 'query_arduino' in debug:
                            ##printdatetime.datetime.now().ctime() + '    creating new database entry for Element: ' + eid
                            log2file('query_arduino', datetime.datetime.now().ctime() + '    creating new database entry for Element: ' + eid + '\n')
                        e = Element(elementid=eid)
                        for eltype in ElementTypes.objects.all():
                            if eltype.designator == edes:
                                e.name = eltype.name + ' ' + eid
                                e.etype = eltype.name
                                e.eunits = eltype.units
                                if 'query_arduino' in debug:
                                    ##printdatetime.datetime.now().ctime() + '    accessing Group: ' + mygroup + ', with name: ' + g.name
                                    log2file('query_arduino', datetime.datetime.now().ctime() + '    accessing Group: ' + mygroup + ', with name: ' + g.name + '\n')
                                g = Group.objects.get(groupid = mygroup)
                                e.group = g
                                e.save()
                                
                                
                if 'query_arduino' in debug:
                    ##printdatetime.datetime.now().ctime() + '    getting actuator IDs'
                    log2file('query_arduino', datetime.datetime.now().ctime() + '    getting actuator IDs' + '\n')               
                aresp = arduino_serial(ser, mygroup, '13!')
                aresp = aresp.split('/')
                grpid = aresp[0]
                for edat in aresp[1].split(','):
                    eid = edat.split(':')[0]
                    edes = eid[1:3]
                    elval = edat.split(':')[1]
                    if 'query_arduino' in debug: 
                        ##printdatetime.datetime.now().ctime() + '    eid: ' + eid + ', val= ' + elval
                        log2file('query_arduino', datetime.datetime.now().ctime() + '    eid: ' + eid + ', val= ' + elval + '\n')
                    if not Element.objects.filter(elementid=eid).exists():
                        ##printdatetime.datetime.now().ctime() + '    creating new database entry for actuator Element: ' + eid
                        log2file('query_arduino', datetime.datetime.now().ctime() + '    creating new database entry for actuator Element: ' + eid + '\n')
                        e = Element(elementid=eid)
                        for eltype in ElementTypes.objects.all():
                            if eltype.designator == edes:
                                e.name = eltype.name + ' ' + eid
                                e.etype = eltype.name
                                e.eunits = eltype.units
                                ##printdatetime.datetime.now().ctime() + '    accessing Group: ' + mygroup + ', with name: ' + g.name
                                log2file('query_arduino', datetime.datetime.now().ctime() + '    accessing Group: ' + mygroup + ', with name: ' + g.name + '\n')
                                g = Group.objects.get(groupid = mygroup)
                                e.group = g
                                e.save()
                                ##printdatetime.datetime.now().ctime() + '    saved Element ID: ' + eid + ' to postgres'
                                log2file('query_arduino', datetime.datetime.now().ctime() + '    saved Element ID: ' + eid + ' to postgres')

                      

            ser.close()
            thread = threading.Thread(target=arduino_thread, args=(mygroup,))
            thread.start()
            ##printdatetime.datetime.now().ctime() + '    dbgroups: ' + str(dbgroups)
            log2file('query_arduino', datetime.datetime.now().ctime() + '    dbgroups: ' + str(dbgroups))
            reset_queries()
            return True
        else:
        
            reset_queries()
            return False
    except:
        return False  


def find_arduinos():
    context = zmq.Context.instance()
    findcon = context.socket(zmq.PULL)
    findcon.setsockopt(zmq.HWM, 1)	# multiple simultanious serial connectoins break the bus?
    findcon.setsockopt(zmq.LINGER, 100)
    findcon.bind('tcp://127.0.0.1:3061')
    findpoller = zmq.Poller()
    findpoller.register(findcon, zmq.POLLIN)
    ##printdatetime.datetime.now().ctime() + '    starting find_arduinos()'
    log2file('find_arduinos', datetime.datetime.now().ctime() + '    starting find_arduinos()')
    ##time.sleep(20)
    global exitthread
    nextcheck = time.time()
    while not exitthread:
      try:
          fsocks = dict(findpoller.poll(100))
      except KeyboardInterrupt:
          findcon.close()
          context.term()
          return
      if fsocks.get(findcon) == zmq.POLLIN:
          fmessage = findcon.recv()
          if 'find_arduinos' in debug:
              print datetime.datetime.now().ctime() + '    find_arduinos Received: ' + str(fmessage)
              log2file('find_arduinos', datetime.datetime.now().ctime() + '    find_arduinos Received: ' + str(fmessage) + '\n')
          if fmessage == 'exit':
              findcon.close()
              context.term()
              exitthread = True
      if time.time() > nextcheck:
        nextcheck = time.time() + 30
        if 'find_arduinos' in debug:
            log2file('find_arduinos', datetime.datetime.now().ctime() + '    running find_arduinos()')
            print'f_a ' + datetime.datetime.now().ctime() + '    running find_arduinos()'
        openports = list_arduino_ports()
        if 'find_arduinos' in debug:
            log2file('find_arduinos', datetime.datetime.now().ctime() + "    openports = " + str(openports))
            print'f_a ' + datetime.datetime.now().ctime() + "    openports = " + str(openports)
        dbgroups = []
        for i in Group.objects.all():
            gid = i.groupid.encode('ascii','ignore')
            dbgroups.append(gid)
        if 'find_arduinos' in debug:
            log2file('find_arduinos', 'dbgroups = ' + str(dbgroups) + '\n' + 'congroups = ' + str(congroups) + '\n' + 'elementIDs = ' + str(elementIDs) + '\n' + 'groupports = ' + str(groupports))
            print'f_a ' + 'dbgroups = ' + str(dbgroups) + '\n' + 'congroups = ' + str(congroups) + '\n' + 'elementIDs = ' + str(elementIDs) + '\n' + 'groupports = ' + str(groupports)
        if 'find_arduinos' in debug:
            log2file('find_arduinos', datetime.datetime.now().ctime() + '    checking for newly connected arduinos')
            print'f_a ' + datetime.datetime.now().ctime() + '    checking for newly connected arduinos'
        newports = list(openports)
        deactivatedports = []
        for i in groupports:
            thisport = groupports[i]
            if 'find_arduinos' in debug:
                log2file('find_arduinos', datetime.datetime.now().ctime() + '    thisport = ' + thisport)
                print'f_a ' + datetime.datetime.now().ctime() + '    thisport = ' + thisport
            if thisport in openports:
                if 'find_arduinos' in debug:
                    log2file('find_arduinos', datetime.datetime.now().ctime() + '    ' + thisport + ' still connected')
                print'f_a ' + datetime.datetime.now().ctime() + '    ' + thisport + ' still connected'
                newports.remove(thisport)
            else:
                if 'find_arduinos' in debug:
                    log2file('find_arduinos', datetime.datetime.now().ctime() + '    ' + str(groupports[i]) + ' no longer connected')
                    ##print'f_a ' + datetime.datetime.now().ctime() + '    ' + str(groupports[i]) + ' no longer connected'
                    #/////////////////////////////////////////////////////////////////////////////////////////////
                deactivatedports.append(i)
                ##if 'find_arduinos' in debug: ##print'added ' + str(i) + ' to deactivatedports[]'

        if 'find_arduinos' in debug and newports != []:
            log2file('find_arduinos', datetime.datetime.now().ctime() + '    newports = ' + str(newports))
            ##print'f_a ' + datetime.datetime.now().ctime() + '    newports = ' + str(newports)
        for i in newports:
            foundgroup = query_arduino(i)
            if foundgroup:
                if 'find_arduinos' in debug:
                    log2file('find_arduinos', datetime.datetime.now().ctime() + '    added new group at ' + i)
                    ##print'f_a ' + datetime.datetime.now().ctime() + '    added new group at ' + i
        for i in dbgroups:
            if i not in congroups:
                deactivatedports.append(i)
        if 'find_arduinos' in debug and deactivatedports != []:
            log2file('find_arduinos', datetime.datetime.now().ctime() + '    deactivatedports ' + str(deactivatedports))
            ##print'f_a ' + datetime.datetime.now().ctime() + '    deactivatedports ' + str(deactivatedports)
        # check if port already deactivated
        abdports = []
        for g in Group.objects.filter(active=False):
            abdports.append(g.groupid)
        if 'find_arduinos' in debug: 
            ##print'abdports = ' + str(abdports)
            log2file('find_arduinos', datetime.datetime.now().ctime() + 'abdports = ' + str(abdports))
        olddeacports = []
        for i in deactivatedports:
            if i in abdports:
                if 'find_arduinos' in debug: 
                    ##print'found ' + i + 'in abdports'
                    log2file('find_arduinos', datetime.datetime.now().ctime() + 'found ' + i + 'in abdports')
                olddeacports.append(i)
            else:
                if 'find_arduinos' in debug: 
                    ##print'not found ' + i + 'in abdports'
                    log2file('find_arduinos', datetime.datetime.now().ctime() + 'not found ' + i + 'in abdports')
        for i in olddeacports:
            deactivatedports.remove(i)
        ##if 'find_arduinos' in debug: ##print'deactivatedports =  ' + str(deactivatedports)
        try:
            for i in deactivatedports:
                g = Group.objects.get(groupid = i)
                try:
                    if 'find_arduinos' in debug: 
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    deactivating ' + str(g.groupid))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    deactivating ' + str(g.groupid)
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    groupports ' + str(groupports))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    groupports ' + str(groupports)
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    i ' + str(i))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    i ' + str(i)
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    groupports[i] ' + str(groupports[i]))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    groupports[i] ' + str(groupports[i])
                    del groupports[i]
                    if 'find_arduinos' in debug: 
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    removed ' + str(i) + ' from groupports' + '\n' + 'groupports = ' + str(groupports))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    removed ' + str(i) + ' from groupports' + '\n' + 'groupports = ' + str(groupports)
                    congroups.remove(i)
                    if 'find_arduinos' in debug: 
                        log2file('find_arduinos', datetime.datetime.now().ctime() + '    removed ' + str(i) + ' from congroups' + '\n' + 'congroups = ' + str(congroups))
                        ##print'f_a ' + datetime.datetime.now().ctime() + '    removed ' + str(i) + ' from congroups' + '\n' + 'congroups = ' + str(congroups)
                    #//////////////////////////////////////////////////////////////////////////////////////////////////////
                    # Thread not exiting on deactivation. Send kill command to thread !!!!!!!!!!!!!!!!!!!!!!!!!!
                    #//////////////////////////////////////////////////////////////////////////////////////////////////////
                except Exception,e:
                    ##print'f_a ' + 'ERROR3: ' + str(e)
                    log2file('find_arduinos', datetime.datetime.now().ctime() + 'f_a ' + 'ERROR3: ' + str(e))
                g.active = False
                g.save()
                if 'find_arduinos' in debug:
                    log2file('find_arduinos', datetime.datetime.now().ctime() + '    deactivated ' + str(g.groupid))
                    ##print'f_a ' + datetime.datetime.now().ctime() + '    deactivated ' + str(g.groupid)
        except Exception,e:
            ####printe
            log2file('find_arduinos', datetime.datetime.now().ctime() + '  ERROR4:  ' + str(e))
            ##print'f_a ' + datetime.datetime.now().ctime() + '  ERROR4:  ' + str(e)
        global arduinosready
        arduinosready = True
        reset_queries()
        time.sleep(0.1)
    ##findcon.close()
    ##context.term()

def arduino_thread(groupid, context=None):
    arduport = groupports[groupid]
    if 'arduino_thread' in debug:
        ##printdatetime.datetime.now().ctime() + '    starting group ' + groupid + ' thread  @ ' + arduport
        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    starting group ' + groupid + ' thread  @ ' + arduport + '\n')
    threadID = groupid + ' ' + str(randint(0,999))
    ser = serial.Serial(port=arduport, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=0.2)
    context = context or zmq.Context.instance()
    arducon = context.socket(zmq.DEALER)
    arducon.setsockopt(zmq.IDENTITY, groupid)
    arducon.setsockopt(zmq.HWM, 1)	# multiple simultanious serial connectoins break the bus?
    arducon.setsockopt(zmq.LINGER, 100)
    arducon.connect('tcp://127.0.0.1:3006')
    findcon = context.socket(zmq.PUSH)
    findcon.setsockopt(zmq.HWM, 1)	# multiple simultanious serial connectoins break the bus?
    findcon.setsockopt(zmq.LINGER, 100)
    findcon.connect('tcp://127.0.0.1:3061')
    threadpoller = zmq.Poller()
    threadpoller.register(arducon, zmq.POLLIN)
    serialOK = True
    streamerrors = 0
    runcount = 0
    while serialOK:
        if 'arduino_thread' in debug:
             ##printdatetime.datetime.now().ctime() + '    thread ' + threadID + ' running...'
             runcount += 1
             if runcount > 10: # multiply this number by poller delay for time between log "running..." updates
               runcount = 0
               log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    thread ' + threadID + ' running...')
        try:
            asocks = dict(threadpoller.poll(500))
        except KeyboardInterrupt:
            return
        if asocks.get(arducon) == zmq.POLLIN:
            amessage = arducon.recv_multipart()
            if 'arduino_thread' in debug:
                ####printdatetime.datetime.now().ctime() + '    Thread Received: ' + str(amessage)
                ##printstr(time.time()) + '    Thread Received: ' + str(amessage)
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    Thread Received: ' + str(amessage) + '\n')
            newcmd = amessage[0][8:]
            ####print'newcmd' + str(newcmd)
            cmdid = amessage[1]
            
            aresp = ''
            ##if amessage[0] == 'exit':
            if newcmd == 'exit':
                 return
            try:
                aresp = arduino_serial(ser, groupid, newcmd)
            except Exception,e:
                ##printdatetime.datetime.now().ctime() + '    arduino_thread() inner ERROR: ' + groupid + ' at ' + arduport + '\n' + str(e)
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    arduino_thread() inner ERROR: ' + groupid + ' at ' + arduport + '\n' + str(e) + '\n')
            arducon.send_multipart([aresp, cmdid])
            if 'arduino_thread' in debug:
                ####printdatetime.datetime.now().ctime() + '    thread sent back' + aresp + ', ' + cmdid
                ##printstr(time.time()) + '    thread sent back' + aresp + ', ' + cmdid
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    thread sent back: ' + aresp + ', ' + cmdid + '\n')
        try:
            ##print groupid + ' starting ser.readline() ' + str(time.time())
            datastream = ser.readline().strip().split('/')
            if 'arduino_thread' in debug and datastream != ['']:
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    datastream = ' + str(datastream) + '\n')
            ##print groupid + ' ended ser.readline() ' + str(time.time())
            ##if datastream != [''] and len(datastream) != 8:
            ##if datastream != [''] and len(datastream) > 8:
            try:
                if datastream != [''] and len(datastream) > 1:
                    ##if 'arduino_thread' in debug:
                        ##printdatetime.datetime.now().ctime() + '    datastream = ' + str(datastream)
                        ##log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    datastream = ' + str(datastream) + '\n')
                    if datastream[0] == groupid:
                        edatalist = datastream[1].split(',')
                        for edata in edatalist:
                            elementdata = edata.split(':')
                            if len(elementdata[0]) == 8 and elementdata[0][0] == 'e' and is_number(elementdata[1]):
                                ##if 'arduino_thread' in debug:
                                    ##print ''
                                    ##printdatetime.datetime.now().ctime() + '    writing element data ' + str(elementdata) + ' to cache'
                                    ##log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    writing element data ' + str(elementdata) + ' to cache' + '\n')
                                cache.set(elementdata[0], elementdata[1], 60)
                                streamerrors -= 1
                                if streamerrors < 0:
                                    streamerrors = 0
                            else:
                                ##printdatetime.datetime.now().ctime() + '    edata ERROR: ' + str(edata)
                                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    edata ERROR: ' + str(edata) + '\n')
                    else:
                        if 'arduino_thread' in debug:
                            ##printdatetime.datetime.now().ctime() + '    datastream group ID Error: ' + str(datastream)
                            log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    datastream group ID ERROR: ' + str(datastream) + '\n')
            except Exception,e:
                streamerrors += 1
                print '///////////// thread.' + groupid, datetime.datetime.now().ctime() + '    inner datastream ERROR: ' + str(e)
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    inner datastream ERROR: ' + str(e) + '\n')
                if streamerrors > 100:
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    outer datastream ERROR: ' + str(e) + '\n')
                    ser.close()
                    serialOK = False
                    findcon.send('exit')
                    time.sleep(2)
                    fthread = threading.Thread(target=find_arduinos, args=())
                    fthread.start()
            else:
                if 'arduino_thread' in debug and streamerrors > 0:
                    print datetime.datetime.now().ctime() + '   Group ID: ' + groupid + '  streamerrors = ' + str(streamerrors)
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    streamerrors =  ' + str(streamerrors) + '\n')
        except Exception,e:
            if 'arduino_thread' in debug:
                streamerrors += 1
                print '///////////// thread.' + groupid, datetime.datetime.now().ctime() + '    outer datastream ERROR: ' + str(e)
                if streamerrors > 10:
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    outer datastream ERROR: ' + str(e) + '\n')
                    ser.close()
                    serialOK = False
                    findcon.send('exit')
                    time.sleep(2)
                    fthread = threading.Thread(target=find_arduinos, args=())
                    fthread.start()
                
            ##time.sleep(1)
            ##ser.open()
            time.sleep(2)
            ##if debug:
                ####print'/////////////   ' + datetime.datetime.now().ctime() + '    Restarted Serial'
                ##log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    Restarted Serial' + '\n')

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##printdatetime.datetime.now().ctime() + '    Starting usbcon'

sysid = ''
for s in System.objects.all():
    if s.default:
        sysid = s.ID
        break

context = zmq.Context()

threadcon = context.socket(zmq.ROUTER)
threadcon.setsockopt(zmq.RCVTIMEO, 5000)
threadcon.setsockopt(zmq.SNDTIMEO, 5000)
threadcon.setsockopt(zmq.HWM, 40)
threadcon.setsockopt(zmq.LINGER, 2000)
threadcon.bind('tcp://127.0.0.1:3006')

brokercon = context.socket(zmq.ROUTER)
brokercon.setsockopt(zmq.SNDTIMEO, 5000)
brokercon.setsockopt(zmq.RCVTIMEO, 5000)
brokercon.setsockopt(zmq.HWM, 20)
brokercon.setsockopt(zmq.LINGER, 2000)
brokercon.bind('tcp://127.0.0.1:3007')

controlcon = context.socket(zmq.REP)
controlcon.setsockopt(zmq.SNDTIMEO, 5000)
controlcon.setsockopt(zmq.RCVTIMEO, 5000)
controlcon.setsockopt(zmq.HWM, 10)
controlcon.setsockopt(zmq.LINGER, 2000)
controlcon.bind('tcp://127.0.0.1:3008')

mainpoller = zmq.Poller()
mainpoller.register(threadcon, zmq.POLLIN)
mainpoller.register(brokercon, zmq.POLLIN)
mainpoller.register(controlcon, zmq.POLLIN)
if 1 in debug:
    ##printdatetime.datetime.now().ctime() + '    registered sockets with mainpoller'
    log2file('main', datetime.datetime.now().ctime() + '    registered sockets with mainpoller')
dbgroups = []
for i in Group.objects.all():
    dbgroups.append(i.groupid.encode('ascii','ignore'))
##if 1 in debug:
    ##printdatetime.datetime.now().ctime() + '    dbgroups: ' + str(dbgroups)
    
congroups = []
elementIDs = []
groupports = {}
ports = list_arduino_ports()
##if 1 in debug: ##printdatetime.datetime.now().ctime() + '    Found active ports: ' + str(ports)

arduinosready = False
thread = threading.Thread(target=find_arduinos, args=())
thread.start()

threadstarttime = time.time() + 20

while not exitthread:
    socks = dict(mainpoller.poll(2000))
    if socks.get(controlcon) == zmq.POLLIN:
        try:
            control = controlcon.recv_multipart()
            ##if 'controlcon' in debug: ##printdatetime.datetime.now().ctime() + '    received on controlcon: ' + str(control)
            if control == ['exit']:
                controlcon.send('exiting')
                exitthread = 1
            else:
                controlcon.send('ERROR, command not found!')
        except Exception,e:
            ##print ''
            x = 1
            ##printdatetime.datetime.now().ctime() + '    controlcon ERROR: ' + str(e)

    if socks.get(brokercon) == zmq.POLLIN:
        try:
            clientid, groupid, command = brokercon.recv_multipart()
            if 'brokercon' in debug:
                ##printdatetime.datetime.now().ctime() + '    received on brokercon: ' + str([clientid, groupid, command])
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    received on brokercon: ' + str([clientid, groupid, command]) + '\n')
            ##if 'brokercon' in debug: ##printdatetime.datetime.now().ctime() + '    arduinosready = ' + str(arduinosready)
            if arduinosready:
                if groupid in congroups:
                    threadcon.send_multipart([groupid, groupid + command, clientid])
                    if 'brokercon' in debug:
                        ##printdatetime.datetime.now().ctime() + '    sent to thread: ' + str([groupid, command, clientid])
                        log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    sent to thread: ' + str([groupid, command, clientid]) + '\n')
                else:
                    brokercon.send_multipart([clientid, 'ERROR, group not found!'])
                    ##printdatetime.datetime.now().ctime() + '    ERROR, group not found!' + '\n'
                    log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    ERROR, group not found!' + '\n')
            else:
                brokercon.send_multipart([clientid, 'wait'])
                ##printdatetime.datetime.now().ctime() + '    wait' + '\n'
                if 'brokercon' in debug: log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    wait' + '\n')
        except Exception,e:
            ##print datetime.datetime.now().ctime() + '    brokercon ERROR: ' + str(e)
            log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    brokercon ERROR: ' + str(e))
    if socks.get(threadcon) == zmq.POLLIN:
        try:
            groupid, data, clientid = threadcon.recv_multipart()
            if 'arduino_thread' in debug:
                ##printdatetime.datetime.now().ctime() + '    received from thread: ' + str([groupid, data, clientid])
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    threadcon received from thread: ' + str([groupid, data, clientid]) + '\n')
            brokercon.send_multipart([clientid, data])
            if 'arduino_thread' in debug:
                ####printstr(time.time()) + '    sent to frontend client: ' + str([clientid, data])
                ##printdatetime.datetime.now().ctime() + '    sent to frontend client: ' + str([clientid, data])
                log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    threadcon sent to frontend client: ' + str([clientid, data]) + '\n')
        except Exception,e:
            ##printdatetime.datetime.now().ctime() + '    threadcon ERROR: ' + str(e)
            log2file('thread.' + groupid, datetime.datetime.now().ctime() + '    threadcon ERROR: ' + str(e))
    ##if 2 in debug: ##printdatetime.datetime.now().ctime() + '    mainpoller...'
    sys.stdout.flush()


exitthread = 1
