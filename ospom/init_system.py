#!/usr/bin/env python

#####################################################
#               Initialise new System               #
#---------------------------------------------------#
# Usage: new_system.py <emailAddress> <System Type> #
#####################################################

import time, random, string, pickle, sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from django.contrib.auth.models import User

from dash.models import System, SensorHistory, SensorHistoryHourAvg, SensorHistoryHourMin, SensorHistoryHourMax


def randgen(size, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

System.objects.all().delete()

system_id = raw_input("System ID: ")
system_password = raw_input("System Password: ")
system_type = raw_input("System Type: ")
system_name = raw_input("System Name: ")

system = System(ID=system_id,
                password=system_password,
                system_type=system_type,
                name=system_name,
                default_system=True,
                active=True)
system.save()
print 'New system ID: ' + system_id + ' created'

SensorHistory.objects.all().delete()
try:
    SensorHistoryHourAvg.objects.all().delete()
except:
    print 'no SensorHistoryHourAvg found'
try:
    SensorHistoryHourMin.objects.all().delete()
except:
    print 'no SensorHistoryHourMin found'
try:
    SensorHistoryHourMax.objects.all().delete()
except:
    print 'no SensorHistoryHourMax found'

print 'Deleted all historic data'

'''
f = open('/usr/local/etc/stunnel/psk.txt', 'w')
f.write(system_id + ':' + system_password)
f.close()
print 'Created new /usr/local/etc/stunnel/psk.txt'
'''
