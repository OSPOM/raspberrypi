#!/opt/aienv/bin/env python

####################################################
#               Webcam Video Server                #
#--------------------------------------------------#
#
#  - Simplecv captures webcam images
#    - Serves to local network @ 
#  - Intervals are specific to each element, and stored in Redis
#      Redis Keys:
#      - <elementid>interval	interval in seconds as float
#      - <elementid>		current value as float
#  - Values are stored in psql in the 'Historic' model
#  - Entries are accessed through thier 'Element' model foriegn key
#      psql Models:
#      - Hisoric:
#        - element, timestamp, value
#      - Element:
#        - elementid
#
#  - ZeroMQ DEALER binds tcp127.0.0.1:3030 accepts requests for data / commands
#      Data Request Format:
#      - [<get>, <elementID>, <firstTime>, <lastTime>, <datapointQty>]
#      Interval Update Format:
#      - [<set>, <elementID>, <seconds>]
#
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import sys, os, time, zmq, redis, logging, glob, random
from cStringIO import StringIO
from SimpleCV import *
# encryption functions
from Crypto.Cipher import AES
import base64

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level,
                    format='%(asctime)s %(message)s')


PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

cache = redis.Redis(host='localhost', port=6379, db=0)

# Find cameras
available_cams = glob.glob('/dev/video*')
cams = []
for cam in available_cams:
    cams.append(cam[-1:])
logging.debug('cams = ' + str(cams))


# Set camera rotation
try:
    camrot = int(cache.get('ospom_cam_rot'))
except:
    camrot = 0
    cache.set('ospom_cam_rot', camrot)
    
# Set camera resolution
try:
    camx = int(cache.get('ospom_cam_x'))
except:
    camx = 1280
    cache.set('ospom_cam_x', 1280)
try:
    camy = int(cache.get('ospom_cam_y'))
except:
    camy = 720
    cache.set('ospom_cam_y', 720)

try:
    camx_web = int(cache.get('ospom_cam_x_web'))
except:
    camx_web = 1280
    cache.set('ospom_cam_x_web', 1280)
try:
    camy_web = int(cache.get('ospom_cam_y_web'))
except:
    camy_web = 720
    cache.set('ospom_cam_y_web', 720)

# Set camera refresh rates
try:
    local_refresh = float(cache.get('ospom_cam_local_refresh'))
except:
    local_refresh = 0.1
    cache.set('ospom_cam_local_refresh', local_refresh)
try:
    web_refresh = float(cache.get('ospom_cam_web_refresh'))
except:
    web_refresh = 10
    cache.set('ospom_cam_web_refresh', web_refresh)

# Set web upload image compression
try:
    jpeg_quality = int(cache.get('ospom_cam_jpeg_quality'))
except:
    jpeg_quality = 50
    cache.set('ospom_cam_jpeg_quality', jpeg_quality)
    

# Choose a camera
try:
    cam_id = int(cache.get('ospom_cam_id'))
    cam = Camera(cam_id, {"width": camx, "height": camy})
except:
    cam_id = 0
    cache.set('ospom_cam_id', 0)
    cam = Camera(cam_id, {"width": camx, "height": camy})
logging.debug('cam_id = ' + str(cam_id))


logging.info('Starting getimages.py @ ' + str(time.time()))

logging.debug('Camera pixels x: ' + str(camx) + ', y: ' + str(camy))

try:
    img = cam.getImage().rotate(camrot)
except:
    time.sleep(2)
    cam_id = int(random.choice(cams))
    logging.debug('randomly chosing camera ' + str(cam_id))
    cam = Camera(cam_id, {"width": camx, "height": camy})
    img = cam.getImage().rotate(camrot)
    cache.set('ospom_cam_id', cam_id)


# Initialize the camera
##cam = Camera(cam_id, {"width": camx, "height": camy})

# Get temporary connection info from Redis
temp_id = cache.get('temp_id')
temp_pass = cache.get('temp_pass')
logging.debug('temp_id = ' + temp_id + ', temp_pass = ' + temp_pass)

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

local_ip = getNetworkIp()
local_ip += ':8080'
logging.debug('local_ip = ' + str(local_ip))
# start http server to stream images over local network
js = JpegStreamer(local_ip)

context = zmq.Context()
push_socket = context.socket(zmq.PUSH)
push_socket.setsockopt(zmq.SNDTIMEO, 10000)
push_socket.setsockopt(zmq.SNDHWM, 2)
push_socket.connect('tcp://127.0.0.1:3062')  # Stunnel SSL
webcom = context.socket(zmq.DEALER)
webcom.setsockopt(zmq.LINGER, 2000)
webcom.bind('tcp://127.0.0.1:3067')

poller = zmq.Poller()
poller.register(webcom, zmq.POLLIN)

last_time = time.time() - 30
campush_timeout = time.time() + 60
jpeg_quality = 50
last_thumbnail = time.time() - 30

web_active_timeout = time.time()

while True:
    socks = dict(poller.poll(100))
    ##logging.debug(str(time.time()))
    if socks.get(webcom) == zmq.POLLIN:
        msg = pickle.loads(webcom.recv())
        logging.debug('msg = ' + str(msg))
        if msg[0] == 'set_web_refresh':
            try:
                web_refresh = float(msg[1])
                cache.set('ospom_cam_web_refresh', web_refresh)
                web_active_timeout = time.time() + 600
                webcom.send('OK')
                logging.debug('set web refresh rate to ' + str(web_refresh))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_local_refresh':
            try:
                local_refresh = float(msg[1])
                cache.set('ospom_cam_local_refresh', local_refresh)
                webcom.send('OK')
                logging.debug('set local refresh rate to ' + str(local_refresh))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_jpeg_quality':
            try:
                jpeg_quality = int(msg[1])
                cache.set('ospom_cam_jpeg_quality', jpeg_quality)
                webcom.send('OK')
                logging.debug('set jpeg quality to ' + str(jpeg_quality))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_web_xy':
            try:
                camx_web = int(msg[1][0])
                camy_web = int(msg[1][1])
                webcom.send('OK')
                logging.debug('set web cam X = ' + str(camx_web) + ', Y = ' + str(camy_web))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_local_xy':
            try:
                camx = int(msg[1][0])
                camy = int(msg[1][1])
                cache.set('ospom_cam_x', camx)
                cache.set('ospom_cam_y', camy)
                webcom.send('OK')
                logging.debug('set local cam X = ' + str(camx) + ', Y = ' + str(camy))
                logging.info('restarting for camera XY change @ ' + str(time.time()))
                sys.exit(0)
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'get_cam_list':
            try:
                webcom.send(pickle.dumps(cams))
                logging.debug('sent cam list ' + str(cams))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_cam_id':
            try:
                new_id = msg[1]
                if int(new_id) == cam_id:
                    webcom.send('OK')
                    logging.debug('Same ID ' + str(new_id))
                else:
                    cache.set('ospom_cam_id', new_id)
                    webcom.send('OK')
                    logging.debug('set cam ID to ' + str(new_id))
                    sys.exit(0)
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
        elif msg[0] == 'set_cam_rot':
            try:
                rotdelta = int(msg[1][0])
                camrot += rotdelta
                if 0 > camrot:
                    camrot = 360 + camrot
                elif camrot > 359:
                    camrot = camrot - 360
                cache.set('ospom_cam_rot', camrot)
                webcom.send('OK')
                logging.debug('set web cam rotation to ' + str(camrot))
            except Exception, e:
                webcom.send('ERROR: image_push.py ' + str(e))
                logging.debug('ERROR: ' + str(e))
          
    time_now = time.time()
    # Get Image from camera
    img = cam.getImage().rotate(camrot)
    img.save(js, quality=jpeg_quality)
    # Upload image
    if (time_now - web_refresh) > last_time:
        if time_now > web_active_timeout:
            cache.set('ospom_cam_web_refresh', 10)
        last_time = time_now
        if last_time > campush_timeout:
            logging.error('--------heartbeat timeout--------');
            # Restart webcom_client if imagepush fails
            try:
                supervisor_xml.supervisor.stopProcess('webcom_client')
                logging.debug('restarting webcom_client supervisor process')
            except:
                logging.debug('starting webcom_clinet supervisor process')
            supervisor_xml.supervisor.startProcess('webcom_client')
            logging.debug('started webcom_client')
        fp = StringIO()
        img.resize(camx_web, camy_web).getPIL().save(fp, 'JPEG', quality=jpeg_quality)
        data = fp.getvalue().encode("base64")
        logging.debug('Image data length = ' + str(len(data)))
        cipher = AES.new(temp_pass)
        encoded_image = EncodeAES(cipher, data)
        send_time = str(last_time)
        packet = [temp_id, temp_pass, send_time, encoded_image, 'large']
        push_socket.send_multipart(packet)
        logging.debug('image packet sent, size = ' + str(len(str(packet))))
        campush_timeout = last_time + 60
    # Upload thumbnail
    if (time_now - 4) > last_thumbnail:
        last_thumbnail = time_now
        fp = StringIO()
        img.resize(128, 72).getPIL().save(fp, 'JPEG', quality=75)
        data = fp.getvalue().encode("base64")
        logging.debug('Thumbnail data length = ' + str(len(data)))
        cipher = AES.new(temp_pass)
        encoded_image = EncodeAES(cipher, data)
        packet = [temp_id, temp_pass, send_time, encoded_image, 'thumbnail']
        push_socket.send_multipart(packet)
        logging.debug('Thumbnail sent, size = ' + str(len(encoded_image)))
    time.sleep(0.1)
