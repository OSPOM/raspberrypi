#!/usr/bin/env python

#####################################################
#               Initialise new System               #
#---------------------------------------------------#
# Usage: new_system.py <emailAddress> <System Type> #
#####################################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
####################################################


import time, pickle, sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from django.contrib.auth.models import User
from dash.models import System

f = open('/opt/aienv/aos/temp/user.data', 'r')
userdata = pickle.loads(f.read())
f.close()

if User.objects.filter(email = userdata['email']).exists():
    print 'Email address matches database'
    user = User.objects.get(email = userdata['email'])
    user.username = userdata['username']
    print 'username = ' + userdata['username']
    user.password = userdata['password']
    print 'password = ' + userdata['password']
    user.email = userdata['email']
    print 'email = ' + userdata['email']
    user.save()
    print 'Saved User'
else:
    print 'User not found!'

