#!/usr/bin/env python

##############################################
#   USB serial communication with Ardiuno    #
##############################################
#
#  -Arduino Commands, DEALER, 127.0.0.1:3006
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['0!']              get group ID
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -receive requests over ZeroMQ
#  -send request to Arduino over serial
#  -receive response from Arduino over serial
#  -send response back over zmq
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, threading, time, datetime, os, xmlrpclib
import serial, glob, StringIO, string, pickle, csv, json

from random import randint

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries


def send_arduino(serial_com, packet):
    logging.debug('send_arduino: packet = ' + str(packet))
    # If the packet doesn't contain an Arduino Group ID, it is a request for the Group ID
    if len(packet) > 1:
        groupid = packet[0]
        logging.debug('send_arduino: groupid = ' + groupid)
        message = packet[1]
        logging.debug('send_arduino: message = ' + message)
    else:
        message = packet[0]
        logging.debug('send_arduino: message = ' + message)
        if message != '0!':
            logging.debug('send_arduino: PACKET ERROR: ' + message + ' not a valid request')
            return 'PACKET ERROR: ' + message
        
    # Flush serial buffers to clear out any old messages
    serial_com.flushInput()
    serial_com.flushOutput()
    logging.debug('send_arduino: flushed serial buffers')
    aresp_timeout  = time.time() + 5
    try:
        # Handle request for Arduino Group ID ('0!')
        if message == '0!':
            while time.time() < aresp_timeout:
                logging.debug('send_arduino: sending ' + str(message) + ' @ ' + str(time.time()))
                serial_com.write(message)
                logging.debug('send_arduino: sent ' + message + ' to Arduino')
                aresp = serial_com.readline().strip()
                logging.debug('send_arduino: aresp = ' + str(aresp))
                if aresp[:8] == aresp[9:]:
                    logging.debug('send_arduino: Arduino ID: ' + aresp[9:])
                    aresp = aresp[9:]
                    return aresp
                else:
                    serial_com.flushInput()
                    serial_com.flushOutput()
                    logging.debug('send_arduino: message is corrupted, flushed serial buffers and resending message')
            logging.debug('send_arduino: arduino response ERROR 1A, received: ' + aresp)
            return 'ERROR_1A'
        
        # Handle all other requests      
        else:
            while time.time() < aresp_timeout:
                logging.debug('send_arduino: sending ' + str(groupid) + str(message) + ' @ ' + str(time.time()))
                serial_com.write(groupid + message)
                logging.debug('send_arduino: sent ' + str(groupid) + str(message))
                aresp = serial_com.readline().strip()
                logging.debug('send_arduino: Arduino response = ' + aresp)
                if aresp[:8] == groupid:
                    return aresp
                # If the message is corrupted, flush buffers and resend message
                else:
                    time.sleep(0.5)
                    serial_com.flushInput()
                    serial_com.flushOutput()
                    logging.debug('send_arduino: message is corrupted, flushed serial buffers and resending message')
            logging.debug('send_arduino: arduino response ERROR 1B, received: ' + aresp)
            return 'ERROR_1B'
    except:
        logging.debug('send_arduino: arduino response ERROR 2, received: ' + aresp)
        return 'ERROR_2'
    
    
cache = redis.Redis(host='localhost', port=6379, db=0)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# Supervisor control
server = xmlrpclib.Server('http://localhost:9003/RPC2')

pid = os.getpid()
logging.debug('pid ' + str(pid))
sys.stdout.flush()


context = zmq.Context()
brokercom = context.socket(zmq.DEALER)
brokercom.setsockopt(zmq.RCVHWM, 2)
brokercom.setsockopt(zmq.SNDHWM, 2)
brokercom.setsockopt(zmq.LINGER, 100)
brokercom.setsockopt(zmq.RCVTIMEO, 5000)
brokercom.setsockopt(zmq.SNDTIMEO, 5000)
brokercom.setsockopt(zmq.IDENTITY, str(pid))
brokercom.connect('tcp://127.0.0.1:3006')

ardusearch = context.socket(zmq.DEALER)
ardusearch.setsockopt(zmq.RCVHWM, 2)
ardusearch.setsockopt(zmq.SNDHWM, 2)
ardusearch.setsockopt(zmq.LINGER, 100)
ardusearch.setsockopt(zmq.RCVTIMEO, 3000)
ardusearch.setsockopt(zmq.SNDTIMEO, 3000)
ardusearch.setsockopt(zmq.IDENTITY, str(pid))
ardusearch.connect('tcp://127.0.0.1:3016')

logging.debug('connected to ardusearch with ID: ' + str(pid) + ' @ ' + str(time.time()))

apoller = zmq.Poller()
apoller.register(brokercom, zmq.POLLIN)

# Get group ID on ZMQ from ardusearch
timenow = time.time()
timeout = timenow + 10
msgOK = False
groupid = ''
while timenow < timeout and msgOK == False:
    try:
        logging.debug('getting group ID from ardusearch' + ' @ ' + str(time.time()))
        sys.stdout.flush()
        groupid = ardusearch.recv_multipart()[1]
        logging.debug('Received groupid ' + groupid + ' over Zmq @ ' + str(time.time()))
        sys.stdout.flush()
        ardusearch.send(groupid)
        msgOK = True
    except Exception, e:
        logging.debug('groupid Error 1: ' + str(e) + ' @ ' + str(time.time()))
        ardusearch.send('0')
    timenow = time.time()
sys.stdout.flush()


serport = ''
serialOK = False
timeout = time.time() + 60
while serialOK != True:
    try:
        # try to get serial port info from Redis key <groupid>_port
        serport = cache.get(groupid + '_port')
        logging.debug('open_serial: got serport: ' + serport + ' from redis')
        ser = serial.Serial(port=serport, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=1.0)
        time.sleep(3)
        logging.debug('open_serial: started serial com with group: ' + groupid + ', on serial port: ' + serport + ' @ ' + str(time.time()))
        recvid = send_arduino(ser, ['0!']).split('/')[0]
        logging.debug('open_serial: recvid = ' + recvid)
        sys.stdout.flush()
        if recvid == groupid:
            serialOK = True
        else:
            logging.debug('open_serial: ERROR: Arduino groupid = "' + groupid + '"')
            if time.time() > timeout:
                # update Arduino Group status, and exit if no response from Arduino
                cache.delete(groupid)
                logging.debug('open_serial: deleted ' + groupid + ' from redis')
                cache.delete(groupid + '_pid')
                logging.debug('open_serial: deleted ' + groupid + '_pid from redis')
                ##cache.srem('connected_groups', groupid)
                ##logging.debug('open_serial: removed ' + groupid + ' from connected_roups in redis')
                g = Group.objects.get(ID=groupid)
                g.active = False
                g.save()
                reset_queries()
                logging.debug('open_serial: set active = False in psql Group ' + groupid)
                sys.exit()
    except Exception,e:
        ser.close()
        logging.debug('open_serial: Arduino Serial Exception ' + str(e))
        sys.stdout.flush()
        if time.time() > timeout:
            cache.delete(groupid)
            logging.debug('open_serial: deleted ' + groupid + ' from redis')
            cache.delete(groupid + '_pid')
            logging.debug('open_serial: deleted ' + groupid + '_pid from redis')
            cache.srem('congroups', groupid)
            logging.debug('open_serial: removed ' + groupid + ' from congroups in redis')
            g = Group.objects.get(ID=groupid)
            g.active = False
            g.save()
            reset_queries()
            logging.debug('open_serial: set active = False in psql Group ' + groupid)
            sys.exit()
        time.sleep(1)


# Send cached actuator value to arduino
g = Group.objects.get(ID=groupid)
actuators = Actuator.objects.filter(group=g).filter(active=True)
for a in actuators:
    actuator_id = str(a.ID)
    actuator_value = cache.get(actuator_id)
    act_resp = actuator_id + 'W:' + actuator_value + '!'
    arduino_resp = send_arduino(ser, [groupid, act_resp])
    logging.debug('init:   wrote to ' + actuator_id + ' the cached value ' + actuator_value + ', got response: ' + arduino_resp)
    time.sleep(0.25)


sensor_arrays = {}
sensor_averages = {
    'ssm0':120,
    'sam0':60,
    'stfw':20,
    'stfa':10,
    'stfb':20,
    'sph0':20,
}

serial_errors = 0
cycle_count = 0
cycle_start = time.time()
while 1:
    reset_queries()
    cycle_count += 1
    if cycle_count > 1000:
        cycle_duration = time.time() - cycle_start
        cycle_rate = 1000 / cycle_duration
        cycle_count = 0
        cycle_start = time.time()
        print 'cycle_rate = ' + str(cycle_rate) + '/second'
    asocks = dict(apoller.poll(10))
    
    if asocks.get(brokercom) == zmq.POLLIN:
        amessage = brokercom.recv_multipart()
        logging.info('brokercom: ' + str(time.time()) + '    brokercom Received: ' + str(amessage))
        sys.stdout.flush()
        newcmd = amessage[0]
        logging.debug('brokercom: newcmd = ' + str(newcmd))
        
        
        sys.stdout.flush()
        clientid = amessage[1]
        logging.debug('brokercom: clientid = ' + str(clientid))
        web_client = amessage[2]
        logging.debug('brokercom: web_client = ' + str(web_client))
        sys.stdout.flush()
        aresp = ''
        timeout = time.time() + 8
        while timeout > time.time():
            try:
                # Send message to Arduino
                aresp = send_arduino(ser, [groupid, newcmd])
                if newcmd[8:9] == 'W':
                    if aresp[0:8] == groupid and aresp[9:10] == '1':
                        actuator_id = newcmd[0:8]
                        actuator_value = newcmd[10:-1]
                        cache.set(actuator_id, actuator_value)
                        logging.debug('set actuator ' + actuator_id + ' to ' + actuator_value)

                # Send response back to arduino_broker
                brokercom.send_multipart([groupid, aresp, clientid, web_client])
                logging.debug('brokercom: sent ' + str([aresp, clientid, web_client]) + ' back to ardubroker')
                serial_errors = 0
                break
            except Exception,e:
                logging.error('brokercom: ERROR: ' + str(e))
                serial_errors += 1
                # If more than 5 messages in a row receive no response from Arduino, restart serial connection
                if serial_errors > 5:
                    logging.debug('restarting serial connection')
                    sys.exit()
    
    ser_buffer = ser.inWaiting()
    if ser_buffer:
        datastream = ser.readline().strip().split('/')
        ##logging.debug(datetime.datetime.now().ctime() + '    datastream = ' + str(datastream) + '\n')
        try:
            if datastream != ['']:
                if datastream[0] == groupid:
                    if datastream[1] == 'SendActData':
                        g = Group.objects.get(ID=groupid)
                        actuators = Actuator.objects.filter(group=g).filter(active=True)
                        for a in actuators:
                            actuator_id = str(a.ID)
                            actuator_value = cache.get(actuator_id)
                            act_resp = actuator_id + 'W:' + actuator_value + '!'
                            arduino_resp = send_arduino(ser, [groupid, act_resp])
                            logging.debug('datastream: wrote to ' + actuator_id + ' the cached value ' + actuator_value + ', got response: ' + arduino_resp)
                            time.sleep(0.25)
                    sdatalist = datastream[1].split(',')
                    logging.debug('datastream: sdatalist = ' + str(sdatalist))
                    for sdata in sdatalist:
                        sensordata = sdata.split(':')
                        logging.debug('datastream: sensordata = ' + str(sensordata))
                        if len(sensordata[0]) == 8 and sensordata[0][0] == 's' and is_number(sensordata[1]):
                            # Process current sensor data
                            sensor_id = sensordata[0]
                            sensor_type = sensor_id[0:4]
                            sensor_value = sensordata[1]
                            # Check if the value should be averaged
                            if sensor_type in sensor_averages:
                                # Add sensor value to a list in redis for averaging
                                cache.rpush(sensor_id + '_array', sensor_value)
                                if cache.llen(sensor_id + '_array') > sensor_averages[sensor_type]:
                                    cache.lpop(sensor_id + '_array')
                                # Get the array and average all the values
                                sensor_array = cache.lrange(sensor_id + '_array', 0, -1)
                                sensor_value = 0
                                for s in sensor_array:
                                    sensor_value += float(s)
                                sensor_value = round(sensor_value / len(sensor_array), 2)
                            
                            cache.set(sensor_id, sensor_value, 60)
                            logging.debug('datastream: set ' + sensor_id + ', ' + str(sensor_value) + ', 60 in Redis')
                        else:
                            logging.error('datastream:  sdata ERROR: ' + str(sdata))
                            datastream = ser.readline().strip().split('/')
                else:
                    logging.error('datastream:  groupid Error 2: ' + str(datastream))
        except Exception,e:
            logging.error('datastream: Error: ' + str(e) + ' @ ' + str(time.time()))




