##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################

import zmq, time, pickle
# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, Element, Historic
from django.db import reset_queries

debug = 2

elementid = 'etf00003'
datapoints = 10
lasttime = time.time() - 100000
firsttime = lasttime - 10000
element = Element.objects.get(elementid = elementid)
##hdata = Historic.objects.filter(element=element).filter(timestamp__gt=firsttime).filter(timestamp__lt=lasttime)
hdata = Historic.objects.filter(element=element).filter(timestamp__gt=firsttime, timestamp__lt=lasttime)
indata = []
for h in hdata:
    indata.append([h.timestamp, h.value])

timestep = (lasttime - firsttime) / datapoints
if debug: print 'timestep = ' + str(timestep)
msg = [hdata[0].value]
timeslice = [firsttime, firsttime + timestep]
if debug: print 'timeslice = ' + str(timeslice)

for p in range(1, datapoints):
    slicedata = []
    for i in indata:
        if i[0] > float(timeslice[1]):
            print 'found end of first timeslice @' + str(timeslice)
            break
        slicedata.append(i[1])
        indata.remove(i)
    if slicedata:
        if debug: print 'slicedata = ' + str(slicedata)
        hv = 0
        for d in slicedata:
            hv += float(d)
        avgval = hv / len(slicedata)  
        if debug > 1: print 'Average value for timeslice ' + str(p) + ' = ' + str(avgval) + ' out of ' + str(len(slicedata)) + ' datapoints'
        msg += [avgval]
    else:
        msg += [msg[-1]]
        if debug > 1: print 'No new data, using previos value: ' + str(msg[-1])
    timeslice[0] += timestep
    timeslice[1] += timestep

if debug: print 'msg = ' + str(msg)
