##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################

import sys, os, django, time
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Historic, Element


lasttime = time.time()
firsttime = lasttime - 1000
e = Element.objects.get(elementid='etf00004')
hdata = Historic.objects.filter(element=e).filter(timestamp__gt=firsttime, timestamp__lt=lasttime)
hvals = []
for h in hdata:
    hvals.append(h.value)
l60sec = 0
last60 = []
lastval = hvals[0]
howmany = 60
for v in range(howmany):
    last60.append(hvals[v])
minimum = lastval
maximum = lastval
print 'last60 = ' + str(last60)
'''
minimum = hvals[0]
maximum = hvals[0]
'''
for i in last60:
    if i < minimum:
        minimum = i
    elif i > maximum:
        maximum = i
print 'minimum = ' + str(minimum)
print 'maximum = ' +str(maximum)

absmin = minimum
absmax = maximum

for v in hvals[howmany:300]:
    minimum = v
    maximum = v
    for i in last60:
        if i < minimum:
            minimum = i
        elif i > maximum:
            maximum = i
        if i < absmin:
            absmin = i
        if i > absmax:
            absmax = i
        
    #print 'last60[] = ' + str(last60)
    print 'minimum = ' + str(minimum) + '    maximum = ' + str(maximum) + '    range = ' + str(maximum - minimum)
    #print 'len(last60) = ' + str(len(last60))
    last60.pop()
    last60.insert(0,v)

absdelta = absmax - absmin

print 'absmin = ' + str(absmin) + '    absmax = ' + str(absmax) + '    range = ' + str(absdelta)
