#!/opt/ospomenv/bin python

##############################################
#          Find connected Arduinos           #
##############################################
#
#  -check USB for open serial connections
#
#  -read/write to django database + redis
#    -redis contains key:<'elementid_port'>, value:<'/dev/ttyUSB..'>
#    -postgres stores Group ID's and active status
#
#  -if new Arduino found:
#    -create database entry
#    -create new supervisor configuration
#    -update supervisor
#    -start new process
# 
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################



import zmq, redis, time, datetime, xmlrpclib
import serial, glob, StringIO, string, pickle, json

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)


try:
    from secrets import sudo_pass
except:
    logging.error('sudo_pass not found')


program_start_time = time.time()


def list_arduino_ports():
    return glob.glob('/dev/ttyACM[0-9]') + glob.glob('/dev/ttyUSB*')

current_port_list = list_arduino_ports()
logging.debug('arduino_search:    current_port_list = ' + str(current_port_list))

# get previous open port list from Redis
cache = redis.Redis(host='localhost', port=6379, db=0)

from random import randint

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries


server = xmlrpclib.Server('http://localhost:9003/RPC2')



context = zmq.Context()
ardusearch = context.socket(zmq.DEALER)
ardusearch.setsockopt(zmq.RCVHWM, 10)
ardusearch.setsockopt(zmq.SNDHWM, 10)
ardusearch.setsockopt(zmq.LINGER, 100)
ardusearch.setsockopt(zmq.RCVTIMEO, 3000)
ardusearch.setsockopt(zmq.SNDTIMEO, 3000)
ardusearch.bind('tcp://127.0.0.1:3016')
logging.debug('arduino_search:    opened ardusearch Zmq DEALER socket at localhost:3016 @ ' + str(time.time()))

# External requests for Arduinos
brokercom = context.socket(zmq.DEALER)
brokercom.setsockopt(zmq.SNDTIMEO, 10000)
brokercom.setsockopt(zmq.RCVTIMEO, 10000)
brokercom.setsockopt(zmq.SNDHWM, 20)
brokercom.setsockopt(zmq.LINGER, 2000)
brokercom.connect('tcp://127.0.0.1:3007')


def arduino_serial(serial, data):
    logging.debug('arduino_search:    arduino_serial: data = ' + str(data))
    if len(data) > 1:
        groupid = data[0]
        logging.debug('arduino_search:    arduino_serial: groupid = ' + groupid)
        message = data[1]
        logging.debug('arduino_search:    arduino_serial: message = ' + message)
    else:
        message = data[0]
        logging.debug('message = ' + message)
    resendtime = time.time() + 2
    logging.debug('arduino_search:    arduino_serial: resendtime = ' + str(resendtime))
    timeout = time.time() + 15
    logging.debug('arduino_search:    arduino_serial: timeout = ' + str(timeout))
    aresp = -1
    logging.debug('arduino_search:    arduino_serial: aresp = ' + str(aresp))
    messageOK = False
    logging.debug('arduino_search:    arduino_serial: messageOK = ' + str(messageOK))
    
    while serial.inWaiting():
        buffertrash = serial.readline()
        logging.debug('arduino_search:    arduino_serial: buffertrash = ' + str(buffertrash))

    while time.time() < timeout and messageOK != True:
        if message == '0!':
            serial.write(message)
            try:
                aresp = serial.readlines()[0].strip()
                logging.debug('arduino_search:    arduino_serial: aresp = ' + str(aresp))
                if aresp[:8] == aresp[9:]:
                    logging.debug('arduino_search:    arduino_serial: Arduino ID: ' + aresp[9:])
                    messageOK = True
                    aresp = aresp[9:]
                else:
                    logging.error('arduino_search:    arduino_serial: ERROR1A:' + aresp)
                    buffertrash = serial.readline()
                    logging.debug('arduino_search:    arduino_serial: buffertrash2 = ' + str(buffertrash))
                    time.sleep(0.5)
                    serial.write(message)
                    
            except Exception, e:
                logging.error('arduino_search:    arduino_serial: ERROR1B:' + str(e))
        else:
            logging.debug('arduino_search:    arduino_serial: sending ' + str(groupid) + str(message))
            serial.write(str(groupid) + str(message))
            logging.debug('arduino_search:    arduino_serial: wrote ' + str(groupid) + str(message))
            aresp = serial.readline().strip()
            logging.debug('arduino_search:    arduino_serial: Arduino message: ' + aresp)
            arespcheck = False
            arespstart = time.time()
            while time.time() < arespstart + 2 and arespcheck == False:
                try:
                    if aresp[:8] == groupid:
                        messageOK = True
                        arespcheck = True
                    else:
                        logging.error('arduino_search:    arduino_serial: ERROR2:   ' + aresp)
                        aresp = serial.readline().strip()
                except Esception, e:
                    logging.error('arduino_search:    arduino_serial: Error3:  ' + str(e))
    sys.stdout.flush()
    return aresp
    







nextcheck = time.time()
logging.debug('arduino_search:    ' + datetime.datetime.now().ctime() + '    running find_arduinos()')

current_port_list = list_arduino_ports()
logging.debug('arduino_search:    current_port_list = ' + str(current_port_list))

# check PostgreSQL for arduinos listed as actively connected
dbgroups = []  # All active Arduino Groups
dbports = []   # All ports for active Arduino Groups
for i in Group.objects.all().filter(active=True):
    gid = i.ID.encode('ascii','ignore')
    dbgroups.append(gid)
    dbport = cache.get(gid + '_port')
    if dbport != None:
        dbports.append(cache.get(gid + '_port'))
    else:
        logging.debug('arduino_search:    ' + str(gid) + '_port not found in redis')
logging.debug('arduino_search:    dbgroups[] = ' + str(dbgroups))
logging.debug('arduino_search:    dbports[] = ' + str(dbports))

newports = list(current_port_list)
logging.debug('arduino_search:    newports = ' + str(newports))
deactivated_ports = []

sys.stdout.flush()

# Check if arduino groups found in databases are currently connected
if dbports:
    for this_port in dbports:
        logging.debug('arduino_search:    this_port = ' + this_port)
        # If this port currently has an arduino connected to it, check connection status
        if this_port in current_port_list:
            logging.debug('arduino_search:    ' + str(this_port) + ' still connected, find group ID connected to port')
            cacheports = cache.keys('*_port')
            group_id = ''
            for k in cacheports:
                kport = cache.get(k)
                if kport == this_port:
                    group_id = k[:8]
                    logging.debug('arduino_search:    group_id = ' + group_id)
                    break
            timeout = time.time() + 10
            while time.time() < timeout:
                try:
                    logging.debug('arduino_search:    testing connection to Group ' + group_id + ' arducom process')
                    brokercom.send_multipart([group_id, '0!', ''])
                    msgresp = brokercom.recv()
                    logging.debug('arduino_search:    msgresp = ' + str(msgresp))
                    if msgresp == group_id:
                        logging.debug('arduino_search:    Group ' + group_id + ' still connected')
                        newports.remove(this_port)
                        break
                    else:
                        time.sleep(1)
                except Exception, e:
                    logging.error('arduino_search:    !!!!!!!!!!!!!!!!!!!!!!!ardusearch error 1: ' + str(e))
                    deactivated_ports.append(this_port)
                    logging.debug('arduino_search:    added ' + str(this_port) + ' to deactivated_ports[]')
                    cacheports = cache.keys('*_port')
                    logging.debug('arduino_search:    cacheports = ' + str(cacheports))
                    # check for redis port listings matching port no longer found on USB
                    # delete redis key if found and set "active" flag in psql "Group" to False
                    for k in cacheports:
                        kport = cache.get(k)
                        if kport == this_port:
                            this_group = k[:8]
                            logging.debug('arduino_search:    this_group = ' + this_group)
                            cache.delete(k)
                            logging.debug('arduino_search:    deleted ' + k + ' from redis')
                            ####################################################################################
                            
                            
                            # Stop supervisor worker process 
                            worker_pid = str(cache.get(this_group + '_pid'))
                            logging.debug('arduino_search:    worker_pid = ' + worker_pid)              
                            logging.debug('arduino_search:    Stopping arducom.py supervisor process for group ' + this_group + ' at port ' + this_port + ' @ ' + str(time.time()))
                            arducomID = ''
                            for i in server.supervisor.getAllProcessInfo():
                                if str(i['pid']) == worker_pid:
                                    arducomID = i['group'] + ':' + i['name']
                                    break
                            try:
                                server.supervisor.stopProcess(arducomID)
                                cache.set(sensor_id, float(sensor_val))
                                s = Sensor.objects.get(ID=sensor_id)
                                s.active = True
                                s.save()
                                logging.info('arduino_search:    set Sensor ' + sensor_id + ' to Active')
                            except Exception, e:
                                logging.error('arduino_search:    supervisor ERROR ' + str(e))
                            logging.info('arduino_search:    Stopped supervisor process ' + arducomID + '\n')
                            
                            
                            ####################################################################################
                            # Delete old worker process ID to port association from Redis
                            cache.delete(this_group + '_pid')
                            logging.debug('arduino_search:    deleted ' + this_group + '_pid from redis')
                            cache.srem('connected_groups', this_group)
                            logging.debug('arduino_search:    removed ' + this_group + ' from connected_groups in redis')
                            g = Group.objects.get(ID=this_group)
                            g.active = False
                            g.save()
                            logging.debug('arduino_search:    set active = False in psql Group ' + this_group)
                    
        # If this ports arduino has been disonnected, update port status
        else:
            logging.debug('arduino_search:    ' + str(this_port) + ' no longer connected')
            deactivated_ports.append(this_port)
            logging.debug('arduino_search:    added ' + str(this_port) + ' to deactivated_ports[]')
            cacheports = cache.keys('*_port')
            logging.debug('arduino_search:    cacheports = ' + str(cacheports))
            # check for redis port listings matching port no longer found on USB
            # delete redis key if found and set "active" flag in psql "Group" to False
            for k in cacheports:
                kport = cache.get(k)
                if kport == this_port:
                    this_group = k[:8]
                    logging.debug('arduino_search:    this_group = ' + this_group)
                    cache.delete(k)
                    logging.debug('arduino_search:    deleted ' + k + ' from redis')
                    ####################################################################################
                    
                    
                    # Stop supervisor worker process 
                    worker_pid = str(cache.get(this_group + '_pid'))
                    logging.debug('arduino_search:    worker_pid = ' + worker_pid)              
                    logging.debug('arduino_search:    Stopping arducom.py supervisor process for group ' + this_group + ' at port ' + this_port + ' @ ' + str(time.time()))
                    arducomID = ''
                    for i in server.supervisor.getAllProcessInfo():
                        if str(i['pid']) == worker_pid:
                            arducomID = i['group'] + ':' + i['name']
                            break
                    try:
                        server.supervisor.stopProcess(arducomID)
                        '''
                        cache.set(sensor_id, float(sensor_val))
                        s = Sensor.objects.get(ID=sensor_id)
                        s.active = True
                        s.save()
                        logging.info('arduino_search:    set Sensor ' + sensor_id + ' to Active')
                        '''
                    except Exception, e:
                        logging.error('arduino_search:    supervisor ERROR ' + str(e))
                    logging.info('arduino_search:    Stopped supervisor process ' + arducomID + '\n')
                    
                    
                    ####################################################################################
                    # Delete old worker process ID to port association from Redis
                    cache.delete(this_group + '_pid')
                    logging.debug('arduino_search:    deleted ' + this_group + '_pid from redis')
                    cache.srem('connected_groups', this_group)
                    logging.debug('arduino_search:    removed ' + this_group + ' from connected_groups in redis')
                    g = Group.objects.get(ID=this_group)
                    g.active = False
                    g.save()
                    logging.debug('arduino_search:    set active = False in psql Group ' + this_group)
        logging.info('arduino_search:    newports = ' + str(newports))
logging.info('arduino_search:    deactivated_ports[] = ' + str(deactivated_ports))

##time.sleep(60)

debug_pid = ''
debug_mygroup = ''

# check all arduino Groups in Postgres for active status
##for system in System.objects.all():
system = System.objects.filter(default_system=True)[0]
sysid = system.ID
logging.debug('arduino_search:    sysid = ' + sysid)
logging.debug('arduino_search:    Checking ports not found in Redis for new Arduinos')
for p in newports:
    # Open serial connection to port
    logging.info('arduino_search:    checking port: ' + p)
    try:
        ser = serial.Serial(port=p, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=2)
        mygroup = ''
        time.sleep(4)
        # Request Group ID over serial
        aresp = ''
        timeout = time.time() + 15
        while timeout > time.time():
            aresp = arduino_serial(ser, ['0!'])
            logging.debug('arduino_search:    aresp = ' + str(aresp))
            try:
                if len(aresp) == 8 and aresp[0] == 'g':
                    logging.debug('arduino_search:    valid group')
                    break
            except Exception,e:
                logging.error('arduino_search:    arduino communication ERROR: ' + str(e))
            time.sleep(4)
        logging.debug('arduino_search:    aresp = ' + str(aresp))
        # Check if Group ID is valid
        try:
            if len(aresp) == 8 and aresp[0] == 'g':
                logging.debug('arduino_search:    valid group')
                mygroup = str(aresp)
                # Find group type in PostgreSQL
                gdes = mygroup[1:4]
                grouptype = ''
                for gtype in GroupType.objects.all():
                    if gtype.des == gdes:
                        grouptype = gtype.name
                        logging.debug('arduino_search:    found group type: "' + grouptype + '" in database')
                if grouptype == '':
                    logging.debug('arduino_search:    GroupType ' + gdes + ' not found in database')
                # Check if Group has already been registered with the system
                try:
                    g = Group.objects.get(ID = mygroup)
                    g.active = True
                    g.save()
                    logging.debug('arduino_search:    found ' + mygroup + ' in PostgreSQL, setting active flag')
                    logging.info('arduino_search:    mygroup = ' + mygroup)
                    cache.sadd('connected_groups', mygroup)
                except:
                    # Register the new Group
                    logging.debug('arduino_search:    ' + mygroup + ' not found in dbgroups, creating new entry in psql')
                    g = Group(ID = mygroup)
                    g.group_type = grouptype
                    g.name = grouptype + ' ' + mygroup
                    g.active = True
                    g.save()
                    g.system.add(system)
                    g.save()
                    logging.info('arduino_search:    mygroup = ' + mygroup)
                    cache.sadd('arduino_search:    connected_groups', mygroup)
                    try:
                        logging.info('arduino_search:    getting sensor IDs')
                        aresp = arduino_serial(ser, [mygroup, '12!'])
                        aresp = aresp.split('/')
                        logging.debug('arduino_search:    aresp = ' + str(aresp))
                        for edat in aresp[1].split(','):
                            edata = edat.split(':')
                            logging.debug('arduino_search:    edata = '+ str(edata))
                            if edata == ['0']:
                                continue
                            eid = edata[0]
                            logging.debug('arduino_search:    eid = ' + str(eid))
                            edes = eid[1:3]
                            logging.debug('arduino_search:    edes = ' + str(edes))
                            elval = edata[1]
                            logging.debug('arduino_search:    elval = ' + str(elval))
                            if not Sensor.objects.filter(ID=eid).exists():
                                logging.debug('arduino_search:    creating new database entry for Sensor: ' + eid)
                                e = Sensor(ID=eid)
                                try:
                                    sensortype = SensorType.objects.get(des=edes)
                                    logging.debug('arduino_search:    found sensortype ' + edes + ' in database')
                                    e.name = sensortype.name + ' ' + eid
                                    e.etype = sensortype.name
                                    e.eunits = sensortype.units
                                    logging.debug('arduino_search:    accessing Group: ' + mygroup + ', with name: ' + g.name)
                                    g = Group.objects.get(ID = mygroup)
                                    e.group = g
                                    e.save()
                                except:
                                    logging.debug('arduino_search:    sensortype ' + edes + ' NOT FOUND IN DATABASE')
                                    logging.debug('arduino_search:    accessing Group: ' + mygroup + ', with name: ' + g.name)
                                    g = Group.objects.get(ID = mygroup)
                                    e.group = g
                                    e.save()
                                
                        logging.info('arduino_search:    getting actuator IDs')        
                        aresp = arduino_serial(ser, [mygroup, '14!'])
                        aresp = aresp.split('/')
                        if aresp[1] != '0':
                            for edat in aresp[1].split(','):
                                eid = edat.split(':')[0]
                                edes = eid[1:4]
                                elval = edat.split(':')[1]
                                logging.debug('arduino_search:    eid = ' + eid + ', val= ' + elval)
                                if not Actuator.objects.filter(ID=eid).exists():
                                    logging.debug('arduino_search:    creating new database entry for Actuator: ' + eid)
                                    e = Actuator(ID=eid)
                                    try:
                                        actuatortype = ActuatorType.objects.get(des=edes)
                                        logging.debug('arduino_search:    found actuatortype ' + edes + ' in database')
                                        e.name = actuatortype.name + ' ' + eid
                                        e.etype = actuatortype.name
                                        e.eunits = actuatortype.units
                                        logging.debug('arduino_search:    accessing Group: ' + mygroup + ', with name: ' + g.name)
                                        g = Group.objects.get(ID = mygroup)
                                        e.group = g
                                        e.save()
                                    except:
                                        logging.debug('arduino_search:    actuatortype ' + edes + ' NOT FOUND IN DATABASE')
                                        logging.debug('arduino_search:    accessing Group: ' + mygroup + ', with name: ' + g.name)
                                        g = Group.objects.get(ID = mygroup)
                                        e.group = g
                                        e.save()
                    
                    except Exception, e:
                        logging.error('arduino_search:    New Group ERROR: ' + str(e) + ' @ ' + str(time.time()))
                        logging.info('arduino_search:    usb port ' + p + ' no longer connected')
                        cache.srem('arduino_search:    current_port_list', p)
                        logging.debug('arduino_search:    removed ' + p + ' from current_port_list in Redis')
                        ##if Group.objects.filter(ID = mygroup).exists():
                            ##Group.objects.get(ID = mygroup).delete()
                            ##logging.error('Deleted Group: ' + mygroup)
                        ser.close()
                        cache.srem('connected_groups', mygroup)
                        continue
                
                # Get all current sensor and actuator values from Arduino Group
                try:
                    logging.info('arduino_search:    getting active sensor IDs')
                    # Send '10!' to arduino to request sensor values
                    aresp = arduino_serial(ser, [mygroup, '10!'])
                    aresp = aresp.split('/')
                    logging.debug('arduino_search:    aresp = ' + str(aresp))
                    try:
                        for sensor in aresp[1].split(','):
                            sensor_data = sensor.split(':')
                            logging.debug('arduino_search:    sensor_data = ' + str(sensor_data))
                            sensor_id = sensor_data[0]
                            logging.debug('arduino_search:    sensor_id = ' + str(sensor_id))
                            # Arduino returns '0' if no sensors are active
                            if sensor_id == '0':
                                break
                            sensor_des = sensor_id[1:3]
                            logging.debug('arduino_search:    sensor_des = ' + str(sensor_des))
                            sensor_val = sensor_data[1]
                            logging.debug('arduino_search:    sensor_val = ' + str(sensor_val))
                            cache.set(sensor_id, float(sensor_val))
                            logging.debug('arduino_search:    set ' + sensor_id + ' in redis to ' + sensor_val)
                            try:
                                s = Sensor.objects.get(ID=sensor_id)
                                s.active = True
                                s.save()
                                logging.debug('arduino_search:    set Sensor: ' + sensor_id + ' to Active in PostgreSQL')
                            except:
                                s = Sensor(ID=sensor_id)
                                s.active = True
                                s.group = g
                                s.save()
                                logging.debug('arduino_search:    SAVED NEW SENSOR ' + sensor_id + ' in PostgreSQL')
                    except Exception, e:
                        logging.error('arduino_search:    ' + str(time.time()) + '  ' + str(e))

                    logging.info('arduino_search:    getting active actuator IDs')
                    # Send '!13' to arduino to request actuator values   
                    aresp = arduino_serial(ser, [mygroup, '13!'])
                    aresp = aresp.split('/')
                    try:
                        for actuator in aresp[1].split(','):
                            actuator_data = actuator.split(':')
                            logging.debug('arduino_search:    actuator_data = ' + str(actuator_data))
                            if actuator_data == ['0']:
                                continue
                            actuator_id = actuator_data[0]
                            logging.debug('arduino_search:    actuator_id = ' + str(actuator_id))
                            # Arduino returns '0' if no actuators are active
                            ##if actuator_id == '0':
                            if len(actuator_id) < 8:
                                break
                            actuator_des = actuator_id[1:3]
                            logging.debug('arduino_search:    actuator_des = ' + str(actuator_des))
                            actuator_val = actuator_data[1]
                            logging.debug('arduino_search:    actuator_val = ' + str(actuator_val))
                            cache.set(actuator_id, float(actuator_val))
                            logging.debug('arduino_search:    set ' + actuator_id + ' in redis to ' + actuator_val)
                            try:
                                a = Actuator.objects.get(ID=actuator_id)
                                a.active = True
                                a.save()
                                logging.debug('arduino_search:    set Actuator ' + actuator_id + ' to Active')
                            except:
                                a = Actuator(ID=actuator_id)
                                a.active = True
                                a.group = g
                                a.save()
                                logging.debug('arduino_search:    SAVED NEW ACTUATOR ' + actuator_id + ' in PostgreSQL')
                    except Exception, e:
                        logging.error('arduino_search:    ' + str(time.time()) + '  ' + str(e))
                        
                except Exception, e:
                    logging.error('arduino_search:    Group Element active ERROR: ' + str(e) + ' @ ' + str(time.time()))
                    continue
                # Close serial connection
                ser.close()
                time.sleep(2)
                cache.set(mygroup + '_port', p)
                cache.sadd('connected_groups', mygroup)
                # Start Supervisor worker process for Arduino communication
                logging.debug('arduino_search:    Starting arducom.py supervisor process for group ' + mygroup + ' at port ' + p + ' @ ' + str(time.time()))
                arducomID = ''
                for i in server.supervisor.getAllProcessInfo():
                    if i['group'] == 'arduino_worker' and i['statename'] != 'STARTING' and i['statename'] != 'RUNNING':
                        arducomID = i['group'] + ':' + i['name']
                        break
                try:
                    server.supervisor.startProcess(arducomID)
                    '''
                    cache.set(sensor_id, float(sensor_val))
                    s = Sensor.objects.get(ID=sensor_id)
                    s.active = True
                    s.save()
                    logging.info('arduino_search:    set Sensor ' + sensor_id + ' to Active')
                    '''
                except Exception, e:
                    logging.error('arduino_search:    supervisor ERROR ' + str(e))
                arduInfo = server.supervisor.getProcessInfo(arducomID)
                logging.info('arduino_search:    Started supervisor process ' + arducomID + '\n'  + str(arduInfo))
                
                pid = str(arduInfo['pid'])
                cache.set(mygroup + '_pid', pid)
                logging.debug('arduino_search:    set ' + mygroup + '_pid to ' + pid + ' in Redis')
                # Send Arduino Group ID to worker thread
                timenow = time.time()
                timeout = timenow + 10
                msgOK = False
                while timenow < timeout and msgOK == False:
                    timenow = time.time()
                    try:
                        logging.debug('arduino_search:    Sending Group ID ' + mygroup + ' to arducom process at Zmq ID : ' + pid + ' @ ' + str(time.time()))
                        ardusearch.send_multipart([pid, mygroup])
                        msgresp = ardusearch.recv()
                        logging.debug('arduino_search:    msgresp = ' + str(msgresp))
                        if msgresp == mygroup: msgOK = True
                    except Exception, e:
                        logging.error('arduino_search:    ardusearch error 1: ' + str(e))
                
                # Restart historic data storage and retrieval program to recognise new Arduino Group
                try:
                    server.supervisor.stopProcess('datastore')
                    logging.debug('arduino_search:    restarting datastore supervisor process')
                except:
                    logging.debug('arduino_search:    starting datastore supervisor process')
                server.supervisor.startProcess('datastore')
                logging.debug('arduino_search:    started datastore')
                
                
                logging.debug('arduino_search:    dbgroups: ' + str(dbgroups))
        except Exception,e:
            logging.error('arduino_search:    ardusearch error 2: ' + str(e))
        ser.close()
    except Exception,e:
        logging.critical('arduino_search:    ardusearch serial port ERROR:' + str(e))
        try:
            os.system('echo %s|sudo -S %s' % (sudo_pass, 'sudo reboot'))
        except Exception,e:
            logging.critical('arduino_search:    ardusearch password ERROR:' + str(e))
sys.stdout.flush()

# Make a list of all Arduino Groups no longer connected to system
for i in dbgroups:
    if i not in cache.smembers('connected_groups'):
        try:
            port_path = cache.get(i + '_port')
            deactivated_ports.append(i)
            logging.debug('arduino_search:    added ' + i + ' to deactivated_ports!!!!!!!!!!!!!!!')
        except:
            logging.info('arduino_search:    port path ' + port_path + ' not found in redis')
if deactivated_ports != []: logging.debug('initial deactivated_ports = ' + str(deactivated_ports))
# check if Group is already deactivated
deactivated_groups = []
for g in Group.objects.filter(active=False):
    deactivated_groups.append(g.ID)
logging.debug('arduino_search:    deactivated_groups = ' + str(deactivated_groups))
olddeacports = []
for i in deactivated_groups:
    group_port = cache.get(i + '_port')
    if group_port:
        if group_port in deactivated_ports:
            logging.debug('arduino_search:    found ' + group_port + 'in deactivated_ports')
            olddeacports.append(group_port)
        else:
            logging.info('arduino_search:    ' + group_port + ' not found in deactivated_ports')
for i in olddeacports:
   deactivated_ports.remove(i)
logging.debug('arduino_search:    new deactivated_ports =  ' + str(deactivated_ports))
# If not already deactivated, set Groups active = False in PostgreSQL
for i in deactivated_ports:
    try:
        if i == 'RasPi000':
            logging.debug('not deactivating RasPi000')
            continue
        # Find Group ID for this port
        group_id = ''
        for k in cacheports:
            kport = cache.get(k)
            if kport == this_port:
                group_id = k[:8]
                logging.debug('arduino_search:    group_id = ' + group_id)
                break 
        g = Group.objects.get(ID = i)
        g.active = False
        g.save()
        logging.info('arduino_search:    deactivated ' + str(g.ID) + ', i = ' + str(i) + ' @ ' + str(time.time()))
    except Exception,e:
        logging.error('arduino_search:    ' + str(time.time()) + '    ' + str(e))

program_elapsed_time = time.time() - program_start_time
logging.info('arduino_search:    ending ardusearch @ ' + str(time.time()) + ' program_elapsed_time = ' + str(program_elapsed_time))
sys.stdout.flush()
sys.exit()


