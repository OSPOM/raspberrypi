#!/usr/bin/env python

##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################

import zmq, redis, time, datetime
from time import strftime
from subprocess import call

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, Element, Historic

debug = 2
argnum = 0
for a in sys.argv:
    if a[0:1] == '-':
        if a[1:] == 'v':
            debug = int(sys.argv[int(argnum)+1])
        else:
            print 'unknown command: ' + a[0:]
    argnum += 1

cache = redis.Redis(host='localhost', port=6379, db=0)
context = zmq.Context()
brokercon = context.socket(zmq.DEALER)
brokercon.setsockopt(zmq.RCVTIMEO, 3000)
brokercon.setsockopt(zmq.SNDTIMEO, 3000)
brokercon.setsockopt(zmq.LINGER, 2000)
brokercon.connect('tcp://127.0.0.1:3007')

active = Group.objects.filter(active=True)
print 'Active Groups:  ' + str(active)
for group in active:
    if debug: print datetime.datetime.now().ctime() + '  checking ' + str(group.groupid)
    brokercon.send_multipart([str(group.groupid), '10!'])
    if debug: print 'sent "10!"'
    output = ['']
    try:
        output = brokercon.recv_multipart()
    except Exception,e:
        if debug: print 'usbcon error: ' + str(e)
    if debug: print 'usbcon response: ' + str(output)
    if output[0] == 'wait':
        time.sleep(20)
    if output[0][0:8] == str(group.groupid):
        brokercon.send_multipart([str(group.groupid), '11:1000!'])
        arduok = 0
        errcount = 0
        while arduok < 1:
            try:
                output = brokercon.recv_multipart()
            except Exception,e:
                print datetime.datetime.now().ctime() + 'brokercon.recv_multipart() Error:   ' + str(e)
                output = '0'
            if debug: print output
            ##print output[0]
            ##if output[0] == '1' or output[0][0] != 'R':
            if output[0] == '1':
                if debug: print 'received valid response'
                arduok = 1
            else:
                print datetime.datetime.now().ctime() + 'connection ERROR 1! group ' + str(group.groupid)
                errcount += 1
                if errcount > 4:
                    arduok = 1
    else:
        print 'connection ERROR 2! group ' + str(group.groupid)
print 'exited for loop!'
print 'exiting and closing all connections'
brokercon.close()
print 'closed brokercon'
context.term()
print 'terminated context'
sys.exit(0)


