#!/usr/bin/python

# Testing network tools for future addition to webcom_client.py

import subprocess

network_info = subprocess.check_output("nmcli device wifi list", shell=True).split('\n')[1:]

network_data = {}

for network_ap in network_info[:-1]:
    ap_info = network_ap.split("'")[1:]
    ap_id = ap_info.pop(0)
    print 'ap_id = ' + str(ap_id)
    ap_info = ap_info[0].split()
    print 'apinfo = ' + str(ap_info)
    ap_bssid = ap_info[0]
    print 'ap_bssid = ' + ap_bssid
    ap_mode = ap_info[1]
    ap_freq = ap_info[2]
    ap_frequ = ap_info[3]
    ap_rate = ap_info[4]
    ap_rateu = ap_info[5]
    ap_signal = ap_info[6]
    ap_secur = ap_info[7]
    ap_active = ap_info[8]
    print 'ap_mode = ' + ap_mode
    print 'ap_freq = ' + ap_freq + ' ' + ap_frequ
    print 'ap_rate = ' + ap_rate + ' ' + ap_rateu
    print 'ap_signal = ' + ap_signal
    print 'ap_secur = ' + ap_secur
    print 'ap_active = ' + ap_active
    network_data[ap_id] = {
        'ap_bssid': ap_bssid,
        'ap_mode': ap_mode,
        'ap_freq': ap_freq,
        'ap_frequ': ap_frequ,
        'ap_rate': ap_rate,
        'ap_rateu': ap_rateu,
        'ap_signal': ap_signal,
        'ap_active': ap_active,
        'ap_secur':ap_secur
    }

