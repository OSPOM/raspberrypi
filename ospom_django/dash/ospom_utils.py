##############################################
#       License                              #
##############################################
#  Copyright 2016 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


#!/usr/bin/env python

# OSPOM utils
import time, logging
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType, SensorHistory, SensorHistoryHourAvg
from django.db import reset_queries

# Clear historic data from postgres
def del_SensorHistory_time(sensors, time_start, time_end):
    logging.debug('deleting sensors: ' + str(sensors) + ', from: ' + str(time_start) + ', to: ' + str(time_end))
    for sensor in sensors:
        logging.debug('sensor = ' + sensor)
        try:
            s = Sensor.objects.get(ID=sensor)
            SensorHistory.objects.filter(sensor=s).filter(timestamp__gt=time_start).filter(timestamp__lt=time_end).delete()
            logging.debug('data deleted')
        except Exception, e:
            logging.debug(e)

# Clear historic data from postgres
def del_SensorHistory_all():
    logging.debug('deleting all sensors history')
    SensorHistory.objects.all().delete()
    SensorHistoryHourAvg.objects.all().delete()

            
# Clean up historic data in postgres
def clean_SensorHistory_time(sensors, time_start, time_end):
    logging.debug('cleaning data for sensors: ' + str(sensors) + ', from: ' + str(time_start) + ', to: ' + str(time_end))
    for sensor in sensors:
        logging.debug('starting on sensor ' + sensor)
        try:
            s = Sensor.objects.get(ID=sensor)
            ##hdata = SensorHistory.objects.filter(sensor=s).filter(timestamp__gt=time_start).filter(timestamp__lt=time_end)
            ##datapoints = len(hdata)
            ##logging.debug('hdata datapoints = ' + str(datapoints))
            
            step_size = 300
            time_step = time_start
            time_next = time_start + step_size
            while time_step < time_end:
                logging.debug('getting historic data between ' + str(time_step) + ' and ' + str(time_next))
                s_data = SensorHistory.objects.filter(sensor=s).filter(timestamp__gt=time_step).filter(timestamp__lt=time_next)
                logging.debug('sdata = ' + str(s_data))
                s_avg = 0
                s_qty = len(s_data)
                if s_qty:
                    for data_entry in s_data:
                        s_avg += data_entry.value
                        data_entry.delete()
                    if s_avg > 0:
                        s_avg = s_avg / s_qty
                    logging.debug('average value = ' + str(s_avg))
                    data_entry = SensorHistory(sensor=s)
                    data_entry.timestamp = time_step
                    data_entry.value = s_avg
                    data_entry.save()
                    logging.debug('saved to postgres')
                time_step += step_size
                time_next += step_size
                reset_queries()
            
        except Exception, e:
            logging.debug(e)
