#!/usr/bin/env python

import zmq, redis, time, datetime, pickle, sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)


# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import Group, Sensor, SensorHistory, SensorHistoryHourAvg
from django.db import reset_queries

last_time = time.time()
first_time = last_time - (60 * 60)

all_sensors = Sensor.objects.filter(active=True)
for s in all_sensors:
    sensor = Sensor.objects.get(ID=s)
    logging.debug('sensor = ' + str(sensor))
    # Get all datapoint after 'first_time' and before 'last_time
    hdata = SensorHistory.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time)
    logging.debug('hdata datapoints = ' + str(len(hdata)))
    sdata = []
    for h in hdata:
        sdata.append([h.timestamp, h.value])
    # Sort all datapoints into chronological order
    sorted_data = sorted(sdata, key=lambda tstamp: tstamp[0])
    logging.debug('len(sorted_data) = ' + str(len(sorted_data)))
    current_time = sorted_data[0][0]
    logging.debug('current_time = ' + str(current_time))
    last_time = sorted_data[-1][0]
    logging.debug('last_time = ' + str(last_time))
    next_time = current_time + (60 * 60)
    while len(sorted_data) > 0:
        hour_data = []
        logging.debug('processing data between ' + str(current_time) + ' and ' + str(next_time))
        logging.debug('next_time = ' + str(next_time))
        while current_time < next_time:
            try:
                sensor_data = sorted_data.pop(0)
            except:
                break
            sensor_value = sensor_data[1]
            current_time = sensor_data[0]
            hour_data.append(sensor_value)
            logging.debug('added ' + str(sensor_value) + ' to hour_data @ ' + str(current_time))
        current_time = next_time
        next_time = next_time + (60 * 60)
        logging.debug('hour_data = ' + str(hour_data))
        hour_avg = 0
        for value in hour_data:
            hour_avg += value
        hour_avg = hour_avg / len(hour_data)
        logging.debug('hour_avg = ' + str(hour_avg))
        
        # Store value in 'Historic' psql model
        
        h = SensorHistoryHourAvg(sensor=sensor)
        h.timestamp = current_time
        h.value = hour_avg
        h.save()
        
        logging.debug('wrote historic data ' + str(hour_avg) + ' @ ' + str(current_time) + ' to postgres')
        time.sleep(1)
