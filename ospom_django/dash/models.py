##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


from django.db import models
from django.contrib.auth.models import User
import time

class System(models.Model):
    ID = models.CharField(max_length=32, primary_key=True)
    password = models.CharField(max_length=128, default='password')
    user = models.ForeignKey(User, default=1)
    name = models.CharField(max_length=32, default='sysname')
    system_type = models.CharField(max_length=24, default='systype')
    default_system = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    def __unicode__(self):
        return self.ID

class Group(models.Model):
    ID = models.CharField(max_length=8, primary_key=True)
    name = models.CharField(max_length=32, default='newgroup')
    group_type = models.CharField(max_length=24, default='grouptype')
    active = models.BooleanField(default=True)
    ##system = models.ForeignKey(System)
    system = models.ManyToManyField(System)
    def __unicode__(self):
        return self.ID

class Sensor(models.Model):
    ID = models.CharField(max_length=8, primary_key=True)
    name = models.CharField(max_length=32, default='newsensor')
    sensor_type = models.CharField(max_length=24, default='sensortype')
    sensor_units = models.CharField(max_length=12, default='')
    group = models.ForeignKey('Group')
    display_order = models.IntegerField(default=1)
    active = models.BooleanField(default=False)
    def __unicode__(self):
        return self.ID

class Actuator(models.Model):
    ID = models.CharField(max_length=8, primary_key=True)
    name = models.CharField(max_length=32, default='newactuator')
    actuator_type = models.CharField(max_length=24, default='actuatortype')
    actuator_units = models.CharField(max_length=12, default='')
    group = models.ForeignKey('Group')
    display_order = models.IntegerField(default=1)
    active = models.BooleanField(default=False)
    def __unicode__(self):
        return self.ID

class SensorHistory(models.Model):
    sensor = models.ForeignKey('Sensor')
    timestamp = models.FloatField(default=0)
    value = models.FloatField(default=0)
    def __unicode__(self):
        resp = self.sensor.name + ' = ' + str(self.value) + ' @ ' + str(self.timestamp)
        return resp
        
class SensorHistoryHourMin(models.Model):
    sensor = models.ForeignKey('Sensor')
    timestamp = models.FloatField(default=0)
    value = models.FloatField(default=0)
    def __unicode__(self):
        resp = self.sensor.name + ' = ' + str(self.value) + ' @ ' + str(self.timestamp)
        return resp
        
class SensorHistoryHourMax(models.Model):
    sensor = models.ForeignKey('Sensor')
    timestamp = models.FloatField(default=0)
    value = models.FloatField(default=0)
    def __unicode__(self):
        resp = self.sensor.name + ' = ' + str(self.value) + ' @ ' + str(self.timestamp)
        return resp

class SensorHistoryHourAvg(models.Model):
    sensor = models.ForeignKey('Sensor')
    timestamp = models.FloatField(default=0)
    value = models.FloatField(default=0)
    def __unicode__(self):
        resp = self.sensor.name + ' = ' + str(self.value) + ' @ ' + str(self.timestamp)
        return resp
        
class ActuatorHistory(models.Model):
    actuator = models.ForeignKey('Actuator')
    timestamp = models.FloatField(default=0)
    value = models.FloatField(default=0)
    def __unicode__(self):
        resp = self.sensor.name + ' = ' + str(self.value) + ' @ ' + str(self.timestamp)
        return resp

class GroupType(models.Model):
    code_prefix = models.CharField(max_length=4, default='')
    name = models.CharField(max_length=32, default='')
    def __unicode__(self):
        resp = self.code_prefix + ',' + self.name
        return resp

class SensorType(models.Model):
    code_prefix = models.CharField(max_length=4, default='')
    name = models.CharField(max_length=32, default='')
    units = models.CharField(max_length=12, default='units')
    def __unicode__(self):
        resp = self.code_prefix + ',' + self.name + ',' + self.units
        return resp
        
class ActuatorType(models.Model):
    code_prefix = models.CharField(max_length=2, default='')
    name = models.CharField(max_length=32, default='')
    units = models.CharField(max_length=12, default='units')
    def __unicode__(self):
        resp = self.code_prefix + ',' + self.name + ',' + self.units
        return resp
        
