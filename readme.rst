.. image:: https://bitbucket.org/repo/dd4BG5/images/3197025692-ospom.jpg
**All code is Copyright: GNU General Public License V3.**
  * This means that you can use any of the code in this repository any way you like, however your versions must be licensed in the same way, and open/available to public view/use.

.. image:: https://bitbucket.org/repo/dd4BG5/images/3868511435-WebDuiNux.png
*There are two main OSPOM WebDuiNux Repo's, one of the Software running on the Raspberry Pi, and one for the software running on the server.*
*This is the Git Repo for all the software running on the Raspberry Pi (or other linux computer).*




**Contact Info:**
  * Email: Staff@OSPOM.com
  * Website: www.OSPOM.com